package com.cw.app.services;

import com.cw.app.DAO.UserDAO;
import com.cw.app.entities.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
@Component
public class UserService {
    private UserDAO userDAO;

    public void addUser(User user){
        userDAO.saveUser(user);
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public ArrayList<User> getAllUsers(){
        return userDAO.getAllUsers();
    }
}
