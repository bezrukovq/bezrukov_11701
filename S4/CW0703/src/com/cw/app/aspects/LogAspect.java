package com.cw.app.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LogAspect {
    @After("execution(void *.controllers.*(*))")
    public void logUserShow(JoinPoint joinPoint){
        System.out.println("user "+joinPoint.getSignature().getName()+" calling now");
    }
}
