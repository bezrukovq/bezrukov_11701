package com.cw.app.controllers;

import com.cw.app.services.UserService;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/participants")
public class ParticipantsController {
    private UserService userService = (UserService) new ClassPathXmlApplicationContext("spring-config.xml").getBean("userService");

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(ModelMap modelMap){
        modelMap.addAttribute("listOfUsers", userService.getAllUsers());
        return "participants";
    }
}
