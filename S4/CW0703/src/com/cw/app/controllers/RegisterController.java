package com.cw.app.controllers;

import com.cw.app.entities.User;
import com.cw.app.services.UserService;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private UserService userService = (UserService) new ClassPathXmlApplicationContext("spring-config.xml").getBean("userService");

    @RequestMapping(method = RequestMethod.POST)
    public String register(ModelMap modelMap, @RequestParam(value = "fio") String fio,
                           @RequestParam(value = "city") String city,@RequestParam(value = "email") String email) {
        userService.addUser(new User(fio, city, email));
        modelMap.addAttribute("listOfUsers", userService.getAllUsers());
        return "participants";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(ModelMap modelMap) {
        return "register";
    }
}
