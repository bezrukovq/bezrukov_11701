package com.cw.app.DAO;

import com.cw.app.entities.User;

import java.util.ArrayList;
import java.util.List;

public interface UserDAO {
    void saveUser(User user);
    ArrayList<User> getAllUsers();
}
