package com.cw.app.DAO.imlp;

import com.cw.app.DAO.UserDAO;
import com.cw.app.Helper.Helper;
import com.cw.app.entities.User;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Component
public class SimplUserDao implements UserDAO {
    @Override
    public void saveUser(User user) {
        Connection connection = Helper.getConnection();
        PreparedStatement st = null;
        try {
            st = connection.prepareStatement("insert into myuser(fio, city, email) VALUES (?,?,?)");
            st.setString(1, user.getFio());
            st.setString(2, user.getCity());
            st.setString(3, user.getEmail());
            st.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<User> getAllUsers() {
        ArrayList<User> usersList = new ArrayList<>();
        try {
            PreparedStatement st = Helper.getConnection().prepareStatement(
                    "select * from myuser");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                usersList.add(new User(rs.getString("fio"), rs.getString("city"), rs.getString("email")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usersList;
    }
}
