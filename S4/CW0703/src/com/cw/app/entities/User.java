package com.cw.app.entities;

public class User {
    private String fio;
    private String city;
    private String email;

    public User(String fio, String city, String email) {
        this.fio = fio;
        this.city = city;
        this.email = email;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
