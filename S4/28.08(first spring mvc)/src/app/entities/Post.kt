package app.entities

data class Post(
        val text:String,
        val date: String
)
