package app.utils

import java.sql.Connection
import java.sql.SQLException
import java.sql.DriverManager


object Helper {

    private var connection: Connection? = null

    fun getExample(a: Int, b: Int, sign: Char): String {
        var str = ""
        str += if (a < 0) "($a)" else a
        str += sign
        str += if (b < 0) "($b)" else b
        str += '='
        return str
    }

    fun getConnection(): Connection {
        if (connection == null) {
            try {
                Class.forName("org.postgresql.Driver")
            } catch (e: ClassNotFoundException) {
                e.printStackTrace()
            }
            try {
                connection = DriverManager.getConnection(
                        "jdbc:postgresql://localhost:5432/posts",
                        "postgres",
                        "postgres")
            } catch (e: SQLException) {
                e.printStackTrace()
            }
        }
        return connection!!
    }
}
