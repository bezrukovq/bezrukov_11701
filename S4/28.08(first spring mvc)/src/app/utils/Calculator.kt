package app.utils

object Calculator {
    fun calculate(a: Int, b: Int, sign: Char): Int {
        when (sign) {
            '+' -> return a + b
            '-' -> return a - b
            '*' -> return a * b
            '/' -> return a / b
        }
        return 0
    }
}
