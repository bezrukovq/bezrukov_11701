package app.dao.impl

import app.dao.PostDAO
import app.entities.Post
import app.utils.Helper
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

object PostDAO_Impl : PostDAO {

    val connection = Helper.getConnection()

    override fun addPost(text: String) {
        try {
            val st: PreparedStatement = connection.prepareStatement("insert into post(text,date) values (?,now())")
            st.setString(1, text)
            st.executeQuery()
        } catch (e: SQLException) {
            e.printStackTrace();
        }
    }

    override fun getPosts(): ArrayList<Post> {
        val posts: ArrayList<Post> = arrayListOf()
        try {
            val st = connection.prepareStatement("select * from post") as PreparedStatement
            val rs: ResultSet = st.executeQuery()
            while (rs.next())
                posts.add(Post(rs.getString("text"), rs.getString("date")))
        } catch (e: SQLException) {
            e.printStackTrace();
        }
        println("AAAAAAAAAAAAAA"+posts.size)
        return posts
    }
}