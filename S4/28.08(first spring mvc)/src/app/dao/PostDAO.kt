package app.dao

import app.entities.Post

interface PostDAO {
    fun addPost(text:String)
    fun getPosts(): List<Post>
}