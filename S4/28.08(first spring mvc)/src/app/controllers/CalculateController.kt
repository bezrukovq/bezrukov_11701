package app.controllers

import app.utils.Calculator
import app.utils.Helper
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("/calculate")
class CalculateController {


    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun startPage(model: Model): String =
            "calculator"

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun calculate(@RequestParam(value = "first") first: Int, @RequestParam(value = "second") second: Int,
                  @RequestParam(value = "sign") sign: Char,model: Model): String {
        val answer = Calculator.calculate(first, second, sign)
        val example = Helper.getExample(first, second, sign)
        model.addAttribute("number", answer)
        model.addAttribute("example", example)
        return "calculator"
    }
}