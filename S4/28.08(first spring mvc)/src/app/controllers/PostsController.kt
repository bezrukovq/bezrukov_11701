package app.controllers

import app.dao.impl.PostDAO_Impl
import app.entities.Post
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("/wall")
class PostsController {
    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun getWall(model: Model): String {
        model.addAttribute("listOfPosts", PostDAO_Impl.getPosts())
        return "postsWall"
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun postPost(model: Model, @RequestParam(value = "text") text: String): String {
        PostDAO_Impl.addPost(text)
        model.addAttribute("listOfPosts", PostDAO_Impl.getPosts())
        return "postsWall"
    }
}