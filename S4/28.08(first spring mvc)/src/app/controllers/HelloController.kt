package app.controllers

import app.entities.Post
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("/hello")
class HelloController {
    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun printHello(model: Model, @RequestParam(value = "name") name: String): String {
        model.addAttribute("name", name)
        return "hello"
    }

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun getPage(modelMap: ModelMap): String =
            "hello"
}
