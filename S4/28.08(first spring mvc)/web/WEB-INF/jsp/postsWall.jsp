<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: Vladimir
  Date: 03.03.2019
  Time: 23:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form method="post">
    <label title="write your post">
        <input type="text" name="text" autofocus>
    </label>
</form>
<c:forEach items="${listOfPosts}" var="p">
    <ul>
        <li>${p.text}</li>
        <li>${p.date}</li>
    </ul>
</c:forEach>
</body>
</html>
