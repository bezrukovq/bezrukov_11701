package view;

import Entities.User;

import java.util.ArrayList;

public interface View {
    void showUser(User user);
    void showUsers(ArrayList<User> users);
}
