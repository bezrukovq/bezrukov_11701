package view;

import Entities.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

@Component
@Qualifier("HTML_View")
public class HTML_View implements View {

    @Override
    public void showUser(User user) {
        System.out.println("<table><tr><td>Name: "+user.getName()+"   email:"+user.getEmail()+"   id"+ user.getId());
    }

    //            TASK02
    @Override
    public void showUsers(ArrayList<User> users) {
        Method[] methods = users.get(0).getClass().getMethods();
        ArrayList<Method> getMethods = new ArrayList<>();
        for (Method method : methods) {
            if (method.getName().startsWith("get") && !method.getName().equals("getClass")) {
                getMethods.add(method);
            }
        }
        System.out.println("<table>");
        for (User x:users) {
            System.out.println("<tr>");
            for (Method method : getMethods) {
                try {
                    wrapWithTd(method.getName().substring(3));
                    wrapWithTd(method.invoke(x).toString());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("</tr>");
        }
        System.out.println("</table>");
    }

    public void wrapWithTd(String s){
        System.out.println("<td>"+s+"</td>");
    }
}
