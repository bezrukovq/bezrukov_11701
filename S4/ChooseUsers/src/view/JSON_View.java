package view;

import Entities.User;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@Qualifier("JSON_View")
public class JSON_View implements View{

    @Override
    public void showUser(User user) {
        System.out.println(new Gson().toJson(user));
    }

    @Override
    public void showUsers(ArrayList<User> users) {
        System.out.println(new Gson().toJson(users));
    }
}
