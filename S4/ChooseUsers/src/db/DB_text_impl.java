package db;

import Entities.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

@Component
//@Qualifier("DB_text_impl")
public class DB_text_impl implements DB {

    private Scanner scanner;

    private void setUpScanner(){
        try {
            scanner = new Scanner(new File("users.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();
        setUpScanner();
        while (scanner.hasNext()) {
            users.add(new User(scanner.nextInt(), scanner.next(), scanner.next()));
        }
        scanner.close();
        return users;
    }

    @Override
    public User getUser(int id) {
        for(User u: getUsers()){
            if (u.getId() == id)
                return u;
        }
        return null;
    }
}
