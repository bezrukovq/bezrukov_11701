package db;

import Entities.User;
import db.DB;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;


@Component
public class DB_Posgres_impl implements DB {

    @Override
    public ArrayList<User> getUsers() {
        Connection connection = getConnection();
        ArrayList<User> usersList = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(
                    "select * from muser");
            ResultSet rs =st.executeQuery();
            while (rs.next()){
                usersList.add(new User(rs.getInt("id"),rs.getString("name"),rs.getString("email")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usersList;
    }

    @Override
    public User getUser(int id) {
        try {
            Connection connection = getConnection();

            PreparedStatement st = connection.prepareStatement("select * from muser where id=?");
            st.setInt(1,id);
            ResultSet rs = st.executeQuery();
            rs.next();
            return new User(id,rs.getString("name"),rs.getString("email"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Connection getConnection() {
        Connection connection = null;
        try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                connection = DriverManager.getConnection(
                        "jdbc:postgresql://localhost:5432/firstFlyBase",
                        "postgres",
                        "postgres");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return connection;
    }
}
