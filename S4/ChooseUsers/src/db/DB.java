package db;

import Entities.User;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;

public interface DB {
    ArrayList<User> getUsers();
    User getUser(int id);
}
