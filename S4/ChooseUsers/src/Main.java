import app.App;
import db.DB;
import db.DB_Posgres_impl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import view.View;

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config.xml");
        App app = applicationContext.getBean(App.class);

        app.showUsers();
        app.showUser(6);
    }
}
