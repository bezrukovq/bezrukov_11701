package Entities;

public class User {
    private String name;
    private int id;
    private String email;

    public User(int id, String name, String email) {
        this.name = name;
        this.id = id;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }
}
