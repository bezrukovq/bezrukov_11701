create table users (
  id       serial      not null primary key,
  name     varchar(15),
  surname  varchar(30),
  city     varchar(30),
  username varchar(20) not null,
  password varchar(30) not null,
  admin    boolean     not null
);
create table comments (
  id        serial      not null primary key,
  sender_id INTEGER REFERENCES users (id),
  topic     varchar(20) not null,
  text      varchar(300)
);
insert into users (username, password, admin)
values ('Vova', 'ovav', true);
insert into users (username, password, admin)
values ('Aina', 'ania', true);
insert into users (username, password, admin)
values ('Kama', 'amak', false);
insert into users (username, password, admin)
values ('Dima', 'amid', false);
insert into comments (sender_id, topic, text)
values ((select id from users where username = 'Vova'), 'chto-to', 'gde cho-to?');
insert into comments (sender_id, topic, text)
values ((select id from users where username = 'Vova'), 'chto-to', 'tam?');
insert into comments (sender_id, topic, text)
values ((select id from users where username = 'Aina'), 'chto-to', 'gde tam?');
insert into comments (sender_id, topic, text)
values ((select id from users where username = 'Aina'), 'chto-to', 'tam?');

select username, topic, text
from comments,
     users
where users.id = sender_id;