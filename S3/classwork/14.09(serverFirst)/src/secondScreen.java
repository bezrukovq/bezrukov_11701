import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.soap.SAAJResult;
import java.io.IOException;

public class secondScreen extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("name");
        if (user != null) {
            response.getWriter().print("<h1>Hello " + user + " </h1>");
            response.getWriter().print("<a href=\"http://localhost:8081/login\">back</a>");
        } else {
            response.sendRedirect("/login");
        }
    }
}



