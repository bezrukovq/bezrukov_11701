import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HelloServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("name");
        String name = request.getParameter("name");
        if (user != null) {
            response.sendRedirect("/profile"); //как он вообще сюда попал
        } else {
            Pattern pattern = Pattern.compile("^(?=.*[A-Za-z0-9]$)[A-Za-z][A-Za-z\\d.-]{0,19}$");
                /*
    (?=.*[A-Za-z0-9]$) Asserts that the match must ends with a letter or digit.
    [A-Za-z] Must starts with a letter.
    [A-Za-z\d.-]{0,19} matches the chars according to the pattern present inside the char class. And the number of matched chars must be from 0 to 19.
                 */
            Matcher matcher = pattern.matcher(name);
            if (matcher.matches()) {
                session.setAttribute("name", name);
                Cookie cookie = new Cookie("name",name);
                cookie.setMaxAge(60*60);
                response.addCookie(cookie);
                response.sendRedirect("/profile?user"); //переходим в свой профиль
            } else {
                response.sendRedirect("/login");
            }
        }
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("text/html");
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("name");
        if (user != null) {
            response.sendRedirect("/profile"); //зачем авторизованному логиниться
        } else {
            String name = "";
            boolean hasCookies = false;
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("name")) {
                    name = cookie.getValue();
                    hasCookies=true;
                    session.setAttribute("name", name);
                    response.sendRedirect("/login");
                }
            }
            if (!hasCookies) {
                response.getWriter().print("<h1>Hello " + name + " </h1>");
                response.getWriter().print("<form method=\"post\" action=\"/login\">\n" +
                        "        <input type=\"text\" name=\"name\">\n" +
                        "        <input type=\"submit\"  value=\"hi\">\n" +
                        "    </form>");
                response.getWriter().close();
            }
        }
    }
}
