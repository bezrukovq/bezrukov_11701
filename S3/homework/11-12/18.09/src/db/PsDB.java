package db;

import entities.User;
import entities.WallPost;

import java.util.ArrayList;
import java.util.List;

public class PsDB {
    private static List<User> users;
    private static List<WallPost> wallPosts;
    public static List<User> getUsers() {
        if (users == null) {
            users = new ArrayList<>();
            users.add(new User("andrey2000", "edward"));
            users.add(new User("andrey99", "nail"));
        }
        return users;
    }

    public static boolean addUser(User user) {
        users.add(user);
        return true;
    }

    public static List<WallPost> getWallPosts() {
        if(wallPosts==null){
            wallPosts = new ArrayList<>();
            wallPosts.add(new WallPost("wow this site supports wallposts!!!","andrey2000"));
            wallPosts.add(new WallPost("Amazing!!","andrey99"));
        }
        return wallPosts;
    }
    public static void addWallpost(WallPost wallPost){
        wallPosts.add(wallPost);
    }
}
