package DAO;

import entities.User;

public interface UserDAO {
    User getByUsername(String username);
    boolean addUser(User user);

}
