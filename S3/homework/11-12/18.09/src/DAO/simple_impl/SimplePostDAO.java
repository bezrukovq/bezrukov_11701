package DAO.simple_impl;

import DAO.PostDAO;
import db.PsDB;
import entities.WallPost;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SimplePostDAO implements PostDAO {
    @Override
    public void addWallPost(WallPost wallPost) {
        PsDB.addWallpost(wallPost);
    }

    @Override
    public void writeWallposts(HttpServletResponse response, String username) throws IOException {
        List<WallPost> wallPosts = PsDB.getWallPosts();
        response.getWriter().print("<ul>");
        for(WallPost w: wallPosts) {
            if(w.getAuthor().equals(username))
            response.getWriter().print("<li>"+w.getText()+"</li>");
        }
        response.getWriter().print("</ul>");

    }
}
