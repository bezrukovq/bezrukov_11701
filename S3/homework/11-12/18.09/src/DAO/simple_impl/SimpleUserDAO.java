package DAO.simple_impl;

import DAO.UserDAO;
import db.PsDB;
import entities.User;

import java.util.List;

public class SimpleUserDAO implements UserDAO {
    @Override
    public User getByUsername(String username) {
        List<User> users = PsDB.getUsers();
        for (User user: users) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public boolean addUser(User user) {
        return PsDB.addUser(user);

    }

}
