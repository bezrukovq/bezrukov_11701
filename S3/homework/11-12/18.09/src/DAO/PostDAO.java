package DAO;

import entities.WallPost;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface PostDAO {
    void addWallPost(WallPost wallPost);
    void writeWallposts(HttpServletResponse response, String usernmae) throws IOException;
}
