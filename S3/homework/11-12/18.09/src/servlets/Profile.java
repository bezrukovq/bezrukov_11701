package servlets;

import DAO.PostDAO;
import DAO.UserDAO;
import DAO.simple_impl.SimplePostDAO;
import DAO.simple_impl.SimpleUserDAO;
import entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Profile extends HttpServlet {
    PostDAO postDAO = new SimplePostDAO();
    UserDAO userDAO = new SimpleUserDAO();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");
        if (user != null) {
            String userreq = request.getParameter("id");
            if ( userreq==null || userreq.equals(user.getUsername())) {
                response.getWriter().print("<h1>Hello " + user.getUsername() + " </h1>");
                response.getWriter().print("<a href=\"http://localhost:8081/post\">create a post</a>");
                postDAO.writeWallposts(response, user.getUsername());
            } else if (userDAO.getByUsername(userreq) != null) {
                response.getWriter().print("<h1>You're visiting " + userreq + " </h1>");
                response.getWriter().print("<a href=\"http://localhost:8081/profile\">back</a>");
                postDAO.writeWallposts(response, userreq);
            }

        } else {
            response.sendRedirect("/login");
        }
    }
}
