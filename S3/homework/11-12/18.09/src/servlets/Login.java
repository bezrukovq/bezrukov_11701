package servlets;

import entities.User;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Login extends HttpServlet {
    private UserService userService = new UserService();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (userService.getCurrentUser(request) != null) {
            response.sendRedirect("/profile");
        } else {
            User user = userService.authenticate(request);
            if (user != null) {
                userService.authorise(request,user);
                response.sendRedirect("/profile");
            } else {
                response.sendRedirect("/login?err_mess=too_bad_login");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (userService.getCurrentUser(request) != null) {
            response.sendRedirect("/profile");
        } else {
            response.setContentType("text/html");
            response.getWriter().println("<form method='POST' >" +
                    "<input type='text' name='username' />" +
                    "<input type='password' name='password' />" +
                    "<input type='submit' value='login' />" +
                    "</form>");
        }
    }
}
