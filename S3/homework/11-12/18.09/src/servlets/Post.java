package servlets;

import DAO.PostDAO;
import DAO.simple_impl.SimplePostDAO;
import entities.User;
import entities.WallPost;

import javax.jws.soap.SOAPBinding;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Post extends HttpServlet {
    PostDAO postDAO = new SimplePostDAO();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String text =(String) request.getParameter("text");
        User user = (User)request.getSession().getAttribute("name");
        if(text!=null){
            postDAO.addWallPost(new WallPost(text,user.getUsername()));
        }
        response.sendRedirect("/profile");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");
        if (user != null) {
            response.getWriter().print("<h1>" + user.getUsername() + "   is making a post </h1>");
            response.getWriter().println("<form method='POST' >"+
                    "<input type='text' name='text' />" +
                    "<input type='submit' value='post' />" +
                    "</form>");
            response.getWriter().print("<a href=\"http://localhost:8081/profile\">back</a>");

        } else {
            response.sendRedirect("/login");
        }
    }
}
