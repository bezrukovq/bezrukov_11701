package services;

import DAO.UserDAO;
import DAO.simple_impl.SimpleUserDAO;
import entities.User;
import sun.java2d.pipe.SpanShapeRenderer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserService {
    private UserDAO userDAO = new SimpleUserDAO();

    public User getCurrentUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("name");
        return user;
    }

    public void authorise(HttpServletRequest request, User user) {
        HttpSession session = request.getSession();
        session.setAttribute("name", user);

    }
    public User authenticate(HttpServletRequest request) {
        String username = request.getParameter("username");
        if (username != null) {
            User user = userDAO.getByUsername(username);
            if (user == null) {
                return null;
            }
            String password = request.getParameter("password");
            if (password.equals(user.getPassword())) {
                return user;
            } else {
                return null;
            }
        }
        return null;
    }
}
