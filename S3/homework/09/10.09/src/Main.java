import java.io.*;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        String request = "";
        Scanner sc = new Scanner(System.in);
        Pattern p = Pattern.compile("/id(\\d)+");
        Matcher m;
        while (!request.equals("exit")) {
            request = sc.nextLine();
            m = p.matcher(request);
            if (request.equals("/feed")) {
                feed();
            } else if (request.equals("/messages")) {
                messages();
            } else if (m.matches()) {
                String[] i = request.split("/id");
                String id = i[1];
                profile(id);
            } else {
                error404(request);
            }
        }
    }

    private static void profile(String id) {
        Date date = new Date();
        try {
            PrintWriter pw = new PrintWriter("HtmlOut/" + date.getTime() + ".html");
            pw.println("<b>" + id + "</b><br>");
            String user = getUser(id);
            if (!user.equals("")) {
                pw.println("<b>" + getUser(id) + "</b>");
            } else {
                pw.println("<b>User with such ID not found</b>");
            }
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void messages() throws FileNotFoundException {
        Scanner sc1 = new Scanner(new File("src/messages.txt"));
        Date date = new Date();
        String read;
        try {
            PrintWriter pw = new PrintWriter("HtmlOut/" + date.getTime() + ".html");
            pw.println("<ul>\n");
            while (sc1.hasNext()) {
                read = sc1.nextLine();
                String[] s = read.split(" ");
                pw.println("<li>From: " + getUser(s[1]) + "   TO:  " + getUser(s[2]) + "   message: " + sc1.nextLine() + "</li>");
            }
            pw.println("</ul>");
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void feed() throws FileNotFoundException {
        Scanner sc1 = new Scanner(new File("src/feed.txt"));
        Date date = new Date();
        String read;
        try {
            PrintWriter pw = new PrintWriter("HtmlOut/" + date.getTime() + ".html");
            pw.println("<h1></h1>");
            pw.println("<ul>\n");
            while (sc1.hasNext()) {
                read = sc1.nextLine();
                String[] s = read.split(" ");
                pw.println("<li>From: " + getUser(s[1]) + "   feed: " + sc1.nextLine() + "</li>");
            }
            pw.println("</ul>");
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    private static void error404(String request) {
        Date date = new Date();
        try {
            PrintWriter pw = new PrintWriter("HtmlOut/" + date.getTime() + ".html");
            pw.println("<h1>404 not found<h1>");
            pw.println("<b>probably wrong request<b>");
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static String getUser(String id) {
        try {
            Scanner sc2 = new Scanner(new File("src/users.txt"));
            String readl;
            String[] arr = new String[0];
            String idr = "";
            while (sc2.hasNext() && !id.equals(idr)) {
                readl = sc2.nextLine();
                arr = readl.split(" ");
                idr = arr[0];
            }
            if (id.equals(idr))
                return arr[1];
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        }

        return "";
    }
}
