public class ClassA {
    public ClassB b;
    public ClassC c;

    public void setB(ClassB b) {
        this.b = b;
    }

    public void setC(ClassC c) {
        this.c = c;
    }

    public ClassA() {
        this.b = b;
        this.c = c;
    }

    public ClassA(ClassB b, ClassC c) {
        this.b = b;
        this.c = c;
    }

    @Override
    public String toString() {
        return "ClassA{" +
                "b=" + b +
                ", c=" + c +
                '}';
    }
}
