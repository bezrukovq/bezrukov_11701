import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
	
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        try {
            Class aClass = Class.forName("ClassA");
            Class bClass = Class.forName("ClassB");
            Class cClass = Class.forName("ClassC");
            ClassA classAObject = (ClassA) aClass.newInstance();
            ClassB classBObject = (ClassB) bClass.newInstance();
            ClassC classCObject = (ClassC) cClass.newInstance();
            ArrayList<Object> objects = new ArrayList<>();
            objects.add(classAObject);
            objects.add(classBObject);
            objects.add(classCObject);

            for (Object object : objects) {
                Method methods[] = object.getClass().getMethods();
                for (Method method : methods) {
                    if (method.getName().startsWith("set")) {
                        System.out.println(method.getName());
                        Class[] params = method.getParameterTypes();
                        System.out.println(params[0]);
                        for (Class type : params) {
                            for (Object bean : objects) {
                                if (type.getName().equals(bean.getClass().getName())) {
                                    method.invoke(object, bean);
                                }
                            }
                        }
                    }
                }
            }
        } catch (ClassNotFoundException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
