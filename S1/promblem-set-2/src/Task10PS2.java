
/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 2 Task 10
 */

public class Task10PS2 {
    public static void main(String[] args) {
        String[] str = {"vovaa", "vova"};
        int comp = compare(str[0], str[1]);
        if (comp != 0) {
            if (comp < 0) {
                System.out.println("Первая строка раньше");
            } else {
                System.out.println("Вторая строка раньше");
            }
        } else {
            System.out.println("Строки равны");
        }

    }

    public static int compare(String s1, String s2) {
        int n1 = s1.length();
        int n2 = s2.length();
        int min = Math.min(n1, n2);
        for (int i = 0; i < min; i++) {
            char c1 = s1.charAt(i);
            char c2 = s2.charAt(i);
            if (c1 != c2) {
                return c1 - c2;
            }
        }
        return n1 - n2;
    }
}
