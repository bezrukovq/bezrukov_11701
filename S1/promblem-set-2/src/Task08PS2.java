import java.util.Scanner;
/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 2 Task 08
 */
public class Task08PS2 {
    public static int[] a = {0,2, 3, 4, 6, 8, 9};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String s = "";
        backtracking(n, s);
    }

    public static void backtracking(int n, String s) {
        if (s.length() == n) {
            System.out.println(s);
        } else if (s.length() == 0) {
            for (int i = 1; i < a.length; i++) {
                backtracking(n, s + a[i]);
            }
        } else {
            for (int i = 0; i < a.length; i++) {
                if (check(s, a[i])) {
                    backtracking(n, s + a[i]);
                }
            }
        }
    }


    public static boolean check(String s, int i) {
        int[][] arr = {
                {2, 4, 6, 8, 0},
                {3, 6, 9, 0, 0},
                {2, 4, 6, 8, 0},
                {2, 3, 4, 6, 8,9,0},
                {2, 4, 6, 8, 0},
                {3, 6, 9, 0, 0}};
        int k = Character.getNumericValue(s.charAt(s.length() - 1));

        switch (k){
            case 0: k = 4;
            break;
            case 2: k = 0;
            break;
            case 3: k = 1;
                break;
            case 4: k = 2;
                break;
            case 6: k = 3;
                break;
            case 8: k = 4;
                break;
            case 9: k = 5;
                break;
        }

        for (int j = 0; j < 5; j++) {
            if (i == arr[k][j]) {
                return true;
            }
        }
        return false;

    }

}
