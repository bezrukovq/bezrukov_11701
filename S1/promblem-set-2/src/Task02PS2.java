import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 2 Task 02
 */


public class Task02PS2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String n = sc.nextLine();
        Pattern p = Pattern.compile("[+-]?(0|[1-9][0-9]*)([.,][0-9]*(\\([0-9]*[1-9]+[0-9]*\\)|[1-9]))?");
        Matcher m = p.matcher(n);
        System.out.println(m.matches());

    }

}
