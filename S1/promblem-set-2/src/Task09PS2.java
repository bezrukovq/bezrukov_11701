import java.util.Scanner;
/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 2 Task 09
 */
public class Task09PS2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        String s = "";
        backtracking(n, s, k);
    }

    public static void backtracking(int n, String s, int k) {
        if (s.length() == n) {
            System.out.println(s);
        } else if (s.length() == 0) {
            for (char c = 'a'; c <= 'z'; c += 1) {
                backtracking(n, s + c, k);
            }
        } else {
            for (char c = 'a'; c <= 'z'; c += 1) {
                if (Math.abs((int) c - s.charAt(s.length() - 1)) == k) {
                    backtracking(n, s + c, k);
                }
            }
        }
    }
}
