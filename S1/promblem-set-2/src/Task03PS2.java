import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 2 Task 03
 */

public class Task03PS2 {
    public static void main(String[] args) {
        String[] str = {"010101", "11", "00", "0110", "1101"};
        for (int i = 0; i < str.length; i++) {
            String n = str[i];
            Pattern p = Pattern.compile("1+|0+|((10)+1?)|((01)+0?)");
            Matcher m = p.matcher(n);
            if (m.matches()) {
                System.out.println(i);
            }
        }
    }
}
