/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 2 Task 11
 */


public class Task11PS2 {
    public static void main(String[] args) {
        String[] str = {"Rafael", "Mike", "Donatello", "Nikita", "Nikolay", "Afanasiy", "Anatoliy"};
        String forChange = "";
        int comp;
        for (int j = 0; j < str.length - 1; j++) {
            for (int i = str.length - 1; i > 0; i--) {
                comp = compare(str[i], str[i - 1]);
                if (comp < 0) {
                    forChange = str[i];
                    str[i] = str[i - 1];
                    str[i - 1] = forChange;
                }
            }
        }
        for (int j = 0; j < str.length; j++) {
            System.out.println(str[j]);
        }
    }


    public static int compare(String s1, String s2) {
        int n1 = s1.length();
        int n2 = s2.length();
        int min = Math.min(n1, n2);
        for (int i = 0; i < min; i++) {
            char c1 = s1.charAt(i);
            char c2 = s2.charAt(i);
            if (c1 != c2) {
                if (c1 != c2) {
                    return c1 - c2;
                }

            }
        }
        return n1 - n2;
    }
}

