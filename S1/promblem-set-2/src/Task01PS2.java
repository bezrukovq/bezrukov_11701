import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 2 Task 02
 */


public class Task01PS2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String n = sc.nextLine();
        Pattern p = Pattern.compile("((02/27/1978\\s(((0[1-9]||1[1-9]||20):[0-5][0-9])|(21:(([0-2][0-9])|(3[0-5])))))|" +
                "((02/(0[1-9]||1[1-9]||2[0-6]))/1978\\s(0[0-9]||1[1-9]||2[0-3]):[0-5][0-9])|" +
                "((0[4-9]||1[0-2])/(0[1-9]||1[0-9]||2[0-9]||3[0-1])/1237\\s(0[1-9]||1[1-9]||2[0-3]):[0-5][0-9])" +
                "|(03/06/1237\\s((1[2-9]||2[0-3]):[0-5][0-9]))|(03/(0[7-9]||[1-2][1-9]||3[0-1])/1237\\s(0[0-9]||1[0-9]||2[0-3]):[0-5][0-9])|" +
                "((0[1-9]||1[0-2])/(0[1-9]||[1-2][0-9]||3[0-1])/((12[4-9][0-9])||(123[8-9])||(1[3-8][0-9][0-9])||(19[0-6][0-9])||(197[0-7]))" +
                "\\s(0[0-9]||1[0-9]||2[0-3]):[0-5][0-9]))");
        Matcher m = p.matcher(n);
        System.out.println(m.matches());

    }

}
