import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 2 Task 12-13
 */
public class Task12_13PS2 {
    public static int k;
    public static int n;

    public static void GeneratePopulation(String[] population, int n) {
        for (int i = 0; i < population.length; i++) {
            population[i] = GenerateString(n);
        }
    }

    public static String GenerateString(int n) {
        String str = "";
        for (int i = 0; i < n; i++) {
            str += (char) ('a' + Math.random() * 25);
        }
        return str;
    }

    public static void ShowPopulation(String[] population) {
        for (int i = 0; i < population.length; i++) {
            System.out.print(population[i] + getKoeff(population[i]) + "     ");
        }
        System.out.println("");
    }

    //------------------
    public static void Selection(String[] population, String[] childPopulation, int n) {
        do {
            makeChildren(population, childPopulation, n);
            sort(childPopulation);
            System.out.println("Population:");
            ;
            ShowPopulation(population);
            System.out.println("Children");
            ShowPopulation(childPopulation);
            change(population, childPopulation);

        } while (!checkPopulation(population));

    }

    //--------------------
    public static void makeChildren(String[] population, String[] childPopulation, int n) {
        int parent;
        for (int i = 0; i < childPopulation.length; i++) {
            childPopulation[i] = "";
            for (int ch = 0; ch < n; ch++) {
                parent = (int) (Math.random() * 2);
                childPopulation[i] += population[2 * i + parent].charAt(ch);
            }
        }
    }

    public static void sort(String[] population) {
        String forChange = "";
        double fChange;
        double[] koef = new double[population.length];
        for (int i = 0; i < population.length; i++) {
            koef[i] = getKoeff(population[i]);
        }
        for (int i = population.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (koef[j] < koef[j + 1]) {
                    forChange = population[j];
                    population[j] = population[j + 1];
                    population[j + 1] = forChange;
                    fChange = koef[j];
                    koef[j] = koef[j + 1];
                    koef[j + 1] = fChange;
                }
            }
        }

    }

    public static void change(String[] population, String[] childPopulation) {
        String forChange;
        for (int i = 0; i < childPopulation.length; i++) {
            for (int j = 0; j < population.length && i < childPopulation.length; j++) {
                if (getKoeff(childPopulation[i]) > getKoeff(population[j])) {
                    forChange = population[j];
                    population[j] = childPopulation[i];
                    childPopulation[i] = forChange;
                    i++;
                }
            }
        }
    }

    public static boolean checkPopulation(String[] population) {
        boolean check = true;
        for (String aPopulation : population) {
            check &= getKoeff(aPopulation) > 98.0;
        }
        return check;
    }

    public static double getKoeff(String population) {
        int numTrue = 0;
        int a;
        int b;
        for (int i = 0; i < population.length() - 1; i++) {
            a = (int) population.charAt(i);
            b = (int) population.charAt(i + 1);
            if (Math.abs(a - b) == k) {
                numTrue += (100/(n-1));
            }
        }

        return (double) numTrue;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        k = sc.nextInt();
        int number = 90;
        String[] population = new String[number];
        String[] childPopulation = new String[number / 2];
        GeneratePopulation(population, n);
        sort(population);
        Selection(population, childPopulation, n);
        System.out.println("   FINAL    ");
        System.out.println("Population:");
        ShowPopulation(population);


    }
}
