import java.util.Scanner;

import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 20
 */

public class Task20PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int l = sc.nextInt();
        boolean checking = false;
        int[][] mat = new int[l][l];
        for (int j = 0; j < l; j++) {
            for (int i = 0; i < l; i++) {
                mat[j][i] = sc.nextInt();
            }
        }

        for (int k = 0; k < l - 1; k++) {
            for (int j = k + 1; j < l; j++) {
                if (mat[k][k] == 0) {
                    for (int str = 0; str < l - k - 1 && !checking; str++) {
                        if (mat[l - 1 - str][k] != 0) {
                            changeStrings(mat, k, l);
                            checking = true;
                        }
                    }
                    if (checking) {
                        checking = false;
                        j--;
                    }
                } else {
                    for (int i = k + 1; i < l; i++) {
                        mat[j][i] = mat[k][k] * mat[j][i] - mat[k][i] * mat[j][k];
                    }
                    mat[j][k] = 0;
                }

            }
            for (int i = 0; i < l; i++) {
                for (int n : mat[i]) {
                    System.out.print(n);
                    System.out.print("  ");
                }
                System.out.println(" ");
            }
            System.out.println(" ");
        }
    }

    public static void changeStrings(int[][] mat, int k, int l) {
        int forChange;
        for (int i = 0; i < l - 1; i++) {
            forChange = mat[k][i];
            mat[k][i] = mat[l - 1][i];
            mat[l - 1][i] = forChange;
        }
    }
}