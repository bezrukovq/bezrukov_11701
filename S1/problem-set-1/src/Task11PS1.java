/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 11
 */

import java.util.Scanner;


public class Task11PS1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int countMax = 0;
        int n = 4;
        int[] arr = new int[n];
        for (int j = 0; j < n; j++) {
            arr[j] = sc.nextInt();
        }
        if (arr[0] > arr[1] && checkChet(arr[0])) {
            countMax++;
        }
        if (arr[n - 1] > arr[n - 2] && checkChet(arr[n - 1])) {
            countMax++;
        }
        for (int i = 1; i < n - 1 && countMax !=3; i++) {
            if (checkLocalMax(arr[i], arr[i - 1], arr[i + 1]) && checkChet(arr[i])) {
                countMax++;
            }
        }
        System.out.println(countMax == 2);
    }

    public static boolean checkLocalMax(int num, int preNum, int nextNum) {
        return num > nextNum && num > preNum;
    }

    public static boolean checkChet(int x) {
        return x % 2 == 0;
    }

}