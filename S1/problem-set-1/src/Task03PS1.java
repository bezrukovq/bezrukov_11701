import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 03
 */

public class Task03PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double n = sc.nextDouble();
        double step = 1;
        double res = 1, prevres = 1;
        final double EPS = 0.000001;
        while (!(res * res - n < EPS && res * res - n > -EPS)) {
            if (res * res > n) {
                res = prevres;
                step /= 10;
            } else {
                prevres = res;
                res += step;
            }
        }
        res = (int)(res*1000000);
        res /= 1000000;
        System.out.println(res);
    }
}
