import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 19
 */


public class Task19PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("введите размер квадратной матрицы");
        int n = sc.nextInt();
        int size = 2 * n + 1;
        int k = 0;
        int[][] Arr = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++)
                Arr[i][j] = sc.nextInt();
        }
        for (int i = 0; i < size / 2; i++) {
            k++;
            for (int j = k; j < size - k; j++) {
                Arr[i][j] = 0;
                Arr[size - i - 1][j] = 0;
            }

        }


        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++)
                System.out.print(Arr[i][j]);
            System.out.println("");
        }

    }
}
