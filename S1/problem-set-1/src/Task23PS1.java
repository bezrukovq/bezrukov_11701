import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 23
 */


public class Task23PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);


        System.out.println("введите размер первой матрицы: кол-во строк и столбцов");
        int str1 = sc.nextInt();
        int stb1 = sc.nextInt();
        int[][] firstArr = new int[str1][stb1];
        int forChange;
        for (int i = 0; i < str1; i++) {
            for (int j = 0; j < stb1; j++)
                firstArr[i][j] = sc.nextInt();
        }


        System.out.println("введите размер второй матрицы: кол-во строк и столбцов");
        int str2 = sc.nextInt();
        int stb2 = sc.nextInt();
        int[][] secondArr = new int[str1][stb1];
        for (int i = 0; i < str2; i++) {
            for (int j = 0; j < stb2; j++)
                secondArr[i][j] = sc.nextInt();
        }

        int[][] resyltMatrix = new int[str1][stb2];
        for (int i = 0; i < str1; i++) {
            for (int j = 0; j < stb2; j++) {
                for (int k = 0; k < stb1; k++) {
                    resyltMatrix[i][j] += firstArr[i][k] * secondArr[k][j];
                }
            }
        }
        for (int i = 0; i < str1; i++) {
            for (int j = 0; j < stb2; j++)
                System.out.println(resyltMatrix[i][j]);
        }

    }
}
