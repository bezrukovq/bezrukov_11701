/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 05
 */

import java.util.Scanner;

public class Task05PS1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);


        int n = sc.nextInt();
        int q = 1;
        int mf = 2;
        int chisl = 1;
        double w = 0.5;
        for (int i = 2; i <= n; i++) {
            chisl *= i - 1;
            mf *= 2 * mf * (2 * mf - 1);
            w += (double) (chisl * chisl) / mf;
        }
        System.out.println("w=" + w);
    }

}
