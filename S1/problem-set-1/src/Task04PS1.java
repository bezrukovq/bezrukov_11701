/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 04
 */

import java.util.Scanner;

public class Task04PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int delitel = 2;
        while (n > 1) {
            if (n % delitel == 0) {
                n /= delitel;
                System.out.println(delitel);
            } else
                delitel++;

        }
    }
}

