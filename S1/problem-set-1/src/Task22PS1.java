import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 22
 */

public class Task22PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("введите размер квадратной матрицы");
        int size = sc.nextInt();
        int max = 0;
        int[][] Arr = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++)
                Arr[i][j] = sc.nextInt();
        }

        for (int k = 0; k < size; k++) {
            max += Arr[k][k];
        }
        int forCheck;
        int j;
        for (int i=1; i<size; i++) {
            forCheck = 0;
            j = i;
            for (int k = 0; j < size; k++) {
                forCheck += Arr[k][j];
                j++;
            }
            j  =j-i;
            for (int k = 0; j < size; k++) {
                forCheck += Arr[j][k];
                j++;
            }
            if (forCheck>max){
                max = forCheck;
            }
        }
        System.out.println(max);
    }
}
