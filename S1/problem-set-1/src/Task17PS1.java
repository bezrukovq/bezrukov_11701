import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 17
 */

public class Task17PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        int forChange;
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        for (int i = 0; i < n / 2; i++) {
            forChange = a[i];
            a[i] = a[n - i - 1];
            a[n - i - 1] = forChange;
        }
        for (int i = 0; i < n; i++) {
            System.out.println(a[i]);

        }

    }
}
