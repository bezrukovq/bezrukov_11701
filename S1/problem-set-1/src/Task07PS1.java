import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 07
 */

public class Task07PS1 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int r = sc.nextInt();
        double rad1 = 0.25 * r * (4 - 1.41421356237);
        double rad2 = 0.25 * r * (4 + 1.41421356237);

        for (int i = 0; i <= 2 * r; i++) {

            for (int j = 0; j <= 2 * r; j++) {

                if (((i - r) * (i - r) + (j - r) * (j - r)) > r * r) {
                    System.out.print(".");
                } else if (check(i, j, rad1, rad2, r)) {
                    System.out.print("0");
                } else {
                    System.out.print("*");
                }
            }
            System.out.println("");
        }
    }

    public static boolean check(int i, int j, double rad1, double rad2, int r) {
        return (((i - rad1) * (i - rad1) + (j - rad1) * (j - rad1)) <= (r * r) / 4)
                | (((i - rad2) * (i - rad2) + (j - rad2) * (j - rad2)) > (r * r) / 4 & (i <= j));
    }
}
