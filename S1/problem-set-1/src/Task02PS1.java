/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 02
 */

import java.util.Scanner;

public class Task02PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        int m = sc.nextInt();
        int forChange;
        if (m < k) {
            forChange = k;
            k = m;
            m = forChange;
        }
        k -= k % 3;
        // между k и m
        for (int i = k + 3; i < m; i += 3) {

            System.out.println(i);

        }
    }
}

