import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 24
 */

public class Task24PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер кубической матрицы");
        int n = sc.nextInt();
        boolean checking = true;
        int[][][] Arr = new int[n][n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < n; k++)
                    Arr[i][j][k] = sc.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            checking &= check(Arr, i, n);
        }
        System.out.println(checking);


    }

    public static boolean check(int[][][] Arr, int i, int n) {
        boolean checking = false;
        for (int j = 0; j < n && !checking; j++) {
            checking = checkString(Arr, i, j, n);
        }
        return checking;
    }

    public static boolean checkString(int[][][] Arr, int i, int j, int n) {
        boolean checking = true;
        for (int k = 0; k < n; k++) {
            checking &= (Arr[i][j][k] % 3 == 0);
        }
        return checking;
    }

}
