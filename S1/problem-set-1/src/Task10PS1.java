/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 10
 */
public class Task10PS1 {
    public static void main(String[] args) {
        boolean haveMax = false;
        int n = 4;
        int i = 1;
        int[] arr = new int[n];
        for (int j = 0; j < n; j++) {
            arr[j] = ((int) (Math.random() * 9) + 1);
        }
        if (arr[0] > arr[1] && arr[0] % 2 == 0 || arr[n - 1] > arr[n - 2] && arr[n - 1] % 2 == 0) {
            haveMax = true;
        }
        while (!haveMax && i < n - 1) {
            haveMax = check(arr, i);
            i++;
        }
        System.out.println(haveMax);
    }

    public static boolean check(int[] arr, int i) {
        return arr[i] > arr[i + 1] && arr[i] > arr[i - 1] && arr[i] % 2 == 0;
    }

}
