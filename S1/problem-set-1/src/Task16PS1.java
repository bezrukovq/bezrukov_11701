import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 16
 */

public class Task16PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        int count = 0;
        boolean res;
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        for (int i = 0; i < n && count < 4; i++) {
            if (check(i,a))
                count++;
        }
        if (count == 3)
            res = true;
        else
            res = false;
        System.out.println(res);
    }

    public static boolean check(int i, int[] a) {
        if (a[i] / 10 == 0)
            return true;
        int num = a[i] % 10;
        int forCheck = a[i] / 10;
        int prenum = num;
        while (forCheck > 0) {
            num = forCheck % 10;
            if (num < prenum) {
                return false;
            }
            forCheck /= 10;
        }
        return true;

    }
}
