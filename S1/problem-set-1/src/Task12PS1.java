/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 12
 */

import java.util.Scanner;

public class Task12PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = 0;
        int dec = 1;
        while (n != 0) {
            if (n % 2 == 1) {
                m += (n % 10) * dec;
                dec *= 10;
            }
            n /=10;
        }
        System.out.println(m);
    }
}
