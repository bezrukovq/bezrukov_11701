import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 18
 */

public class Task18PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        int forChange;
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        int k = sc.nextInt();
        for (int j = 0; j<k; j++) {
            for (int i = 0; i < n-1; i++) {
                forChange = a[i];
                a[i] = a[i + 1];
                a[i + 1] = forChange;
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println(a[i]);

        }
    }
}
