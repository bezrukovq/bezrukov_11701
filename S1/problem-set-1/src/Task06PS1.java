/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 06
 */

import java.util.Scanner;

public class Task06PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double r = (double) n - 1;


        for (int i = (int) -r; i <= (int) r; i++) {
            for (int j = (int) -r; j <= (int) r; j++) {
                if (i * i + j * j <= r * r) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }

}
