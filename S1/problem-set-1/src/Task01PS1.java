/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 01
 */

import java.util.Scanner;

public class Task01PS1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < 2 * a - i; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j <= (i + 1) * 2 - 1; j++) {
                System.out.print("*");
            }
            System.out.println("");
        }
        for (int i = 0; i < a; i++) {
            for (int k = 1; k < 3; k++) {
                for (int j = 1; j <= (a - i - 1) * k; j++) {
                    System.out.print(" ");
                }
                System.out.print(" ");
                for (int j = 1; j <= (i + 1) * 2 - 1; j++) {
                    System.out.print("*");
                }
            }

            System.out.println("");
        }
    }
}
