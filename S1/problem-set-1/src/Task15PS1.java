import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 15
 */

public class Task15PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        int count = 0;
        boolean res;
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        for (int i = 0; i < n && count<3; i++) {
            if (chet(i,a)&& lang35(i,a))
                count++;
        }
        if (count == 2)
            res = true;
        else
            res = false;
        System.out.println(res);
    }

    public static boolean lang35(int i, int[] a) {
        int forCheck = a[i];
        int length = 0;
        while (forCheck > 0 && length < 7) {
            forCheck /= 10;
            length++;
        }
        if (length == 3 || length == 5)
            return true;
        else
            return false;

    }

    public static boolean chet(int i, int[] a) {
        int num = a[i] % 10;
        int forCheck = a[i] / 10;
        int prenum = num;
        while (forCheck > 0) {
            num = forCheck % 10;
            if (num % 2 != prenum % 2) {
                return false;
            }
            forCheck /= 10;
        }
        return true;

    }
}
