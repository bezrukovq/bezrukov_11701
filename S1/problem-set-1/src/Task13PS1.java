import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 13
 */

public class Task13PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = 0;
        int dec = 1;
        while (n != 0) {
            m += (n % 10) * dec;
            dec *= 10;
            n -=1;
        }
        System.out.println(m);
    }
}
