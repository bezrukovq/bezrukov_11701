/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 08
 */

import java.util.Scanner;

public class Task08PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        final double eps = 1e-9;
        double a = 1;
        double sum = x;
        double stX = x;
        int k = 1;
        double kf = 1;
        double lowsum;
        while (Math.abs(a) > eps) {
            kf *= (2 * k * (2 * k - 1)) * (-1);
            lowsum = 4 * k + 1;
            stX *= x * x * x * x;
            a = (double) stX / (kf * lowsum);
            sum += a;
            k++;
        }
        System.out.println(sum);

    }
}