import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 1 Task 14
 */

public class Task14PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        int n = sc.nextInt();
        int decimal = 0;
        int st = 1;
        while (n != 0) {
            decimal += (n % 10) * st;
            st *= k;
            n /= 10;
        }
        n = decimal;
        System.out.println(n);
    }
}
