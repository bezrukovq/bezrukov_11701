import java.util.Scanner;
public class Program35a {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		float x = sc.nextFloat();
		float y = sc.nextFloat();
		float z = sc.nextFloat();
		float a = x+y+z;
		float b = x*y*z;
		float max = a > b ? a : b;
		System.out.print(max);  
    }
}