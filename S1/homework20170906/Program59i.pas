import java.util.Scanner;
public class Program59i {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		float x = sc.nextFloat();
		float y = sc.nextFloat();
		if (x<=0 && y<Math.abs(x) && y<2*x+3 && y>x/3-1/3 ||
		x>0 && y<0 && y>x/3-1/3) {
			System.out.println("Принадлежит");
		} else {
			System.out.println("Не принадлежит");
		}
	}
}