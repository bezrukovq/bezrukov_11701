import java.util.regex.*;
public class Time {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("(([01]\\d)|([2][0-3]))((:[0-5][0-9]){2})");
		for ( String n: args){
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
}