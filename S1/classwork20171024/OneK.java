import java.util.regex.*;
public class OneK {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("1\\d{3}");
		for ( String n: args){
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
}