import java.util.regex.*;
public class Number {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("\\+7(-(\\d){3}){2}(-(\\d){2}){2}");
		for ( String n: args){
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
}