import java.util.regex.*;
public class Hardyears {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("((1[5-9]\\d{2})|(143[2-9])|(14[4-9]\\d))|(2[0-2]\\d{2})|(23[0-6]\\d)|(237[0-5])");
		for ( String n: args){
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
}