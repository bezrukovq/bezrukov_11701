import java.util.regex.*;
public class Digits {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("(-?[1-9]\\d*)|0");
		for ( String n: args){
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
}