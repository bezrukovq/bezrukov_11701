import java.util.regex.*;
public class Noperiod {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("-?(0|[1-9]\\d*)\\.\\d*");
		for ( String n: args){
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
}