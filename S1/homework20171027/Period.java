
import java.util.regex.*;
public class Period {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("-?([1-9]\\d*|\\d)\\.\\d*([\\(]\\d+[\\)])\\d*");
		for ( String n: args){
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
}