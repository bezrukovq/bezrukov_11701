import java.util.regex.*;
public class Operator {
	public static void main(String[] args) {
		String ch;
		for ( String n : args){
			int s = 0;
			boolean correct = true;
			for (int i = 0; i< n.length(); i++){
				switch (s) {
					case 0:
						if (Character.isLetter(n.charAt(i))){
							s = 1;
						} else correct= false; 
						break;
					case 1:
						ch = String.valueOf(n.charAt(i));
						if (ch.equals("=")){
							s = 2;
						}
						break;
					case 2: 
						if (Character.isLetter(n.charAt(i))){
							s = 3;
						} else if (Character.isDigit(n.charAt(i))){
							s = 4;
						}
						break;
					case 3:
						ch = String.valueOf(n.charAt(i));
						if (ch.equals(";")) {
							s = 0;
						}
						break;
					case 4:
						ch = String.valueOf(n.charAt(i));
						if (Character.isLetter(n.charAt(i))){
							correct = false;
						} else if (ch.equals(";")){
							s = 0;
						}
						break;
				}
				if (correct == false) {break;}
			}
			correct = (s==0)&&correct;
			System.out.println(correct);
		}
	}
}
