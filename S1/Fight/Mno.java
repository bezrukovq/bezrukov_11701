package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import static com.mygdx.game.Player.setActivePlayers;

public class Monopoly extends ApplicationAdapter {

    final String FONT_CHARS = "абвгдежзийклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyz"
            + "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=()*&.;:,{}\"´`'<>";

    private Cell[] cells = new Cell[40];
    private SpriteBatch batch;
    private Texture img, fonImg, winImg;
    private Texture TurnWindow;
    private Texture[] texArrOfCubes;

    private BitmapFont infAboutMoney, infAboutTurn;

    private Music abrMusic;
    private OrthographicCamera camera;
    private int countPlayers;
    private int dice1 = 0, dice2 = 0;
    private Player[] players;
    private boolean isCreated, playersCreated, turnFinished = true,
            tapEnter, tapSpace, rentpayed = false;
    private int turn;
    private BitmapFont font;

    @Override
    public void create() {
        loadCells();
        font = new BitmapFont(Gdx.files.internal("font.fnt"));
        //  font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 1000, 1000);

        batch = new SpriteBatch();

        infAboutMoney = new BitmapFont();
        infAboutMoney.setColor(Color.BLACK);

        infAboutTurn = new BitmapFont();
        infAboutTurn.setColor(Color.BLACK);

        fonImg = new Texture("spec.jpg");
        winImg = new Texture("win.jpg");
        img = new Texture("1000x1000монополия.jpg");
        TurnWindow = new Texture("table1.jpg");
        texArrOfCubes = new Texture[6];
        texArrOfCubes[0] = new Texture("dice1.png");
        texArrOfCubes[1] = new Texture("dice2.png");
        texArrOfCubes[2] = new Texture("dice3.png");
        texArrOfCubes[3] = new Texture("dice4.png");
        texArrOfCubes[4] = new Texture("dice5.png");
        texArrOfCubes[5] = new Texture("dice6.png");

        abrMusic = Gdx.audio.newMusic(Gdx.files.internal("abrMusic.mp3"));
        abrMusic.setLooping(true);
        abrMusic.play();

    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(img, 0, 0);

        if (isCreated && playersCreated) {
            for (int i = 0; i < 40; i++) {
                if (cells[i].getOwner() >= 0 && cells[i].getOwner() <= 3 && players[cells[i].getOwner()].isActive()) {
                    batch.draw(players[cells[i].getOwner()].getLabelOwner(), cells[i].getX(), cells[i].getY() + 40);
                }
            }
            font.setColor(5, 5, 5, 5);
            font.getData().setScale(0.3f);
            GlyphLayout layout = new GlyphLayout(font,  "Turn :");
            font.draw(batch, layout, 200, 800);
            //infAboutTurn.draw(batch, "Turn :", 200, 800);
            batch.draw(players[turn].getTexture(), 240, 770);

        }

        if (!isCreated) {
            batch.draw(fonImg, 0, 0);
        }

        if (dice1 > 0) {
            batch.draw(texArrOfCubes[dice1 - 1], 200, 200);
        }

        if (dice2 > 0) {
            batch.draw(texArrOfCubes[dice2 - 1], 400, 200);
        }

        if (isCreated && !turnFinished) {
            batch.draw(TurnWindow, 500, 500);

            //infAboutMoney.draw(batch, "Pl " + (turn + 1) + ": have " + players[turn].getMoney(), 200, 740);
            GlyphLayout layout = new GlyphLayout(font,  "Player " + (turn + 1) + ": have " + players[turn].getMoney()+"$");
            font.draw(batch, layout, 200, 750);

            tapEnter = Gdx.input.isKeyJustPressed(Input.Keys.ENTER);
            tapSpace = Gdx.input.isKeyJustPressed(Input.Keys.SPACE);

            if (!rentpayed) {
                if (players[turn].getCellnumber() - players[turn].getPreviousCellnumber() < 0) {
                    players[turn].setMoney(players[turn].getMoney() + 200);
                   // System.out.println("Pl " + (turn + 1) + ": got 200 money." + " He has " + players[turn].getMoney());
                    layout = new GlyphLayout(font,  "Player " + (turn + 1) + "got 200 money. " +"He has "+ players[turn].getMoney()+"$");
                    font.draw(batch, layout, 400, 750);
                }
                if (checkOwner(cells, players, turn)) {
                    players[turn].payRent(cells, players);
                   // System.out.println("Pl " + (turn + 1) + ": paid rent for the card " + players[turn].getCellnumber()
                     //       + ". Pl " + (turn + 1) + " have " + players[turn].getMoney());
                    layout = new GlyphLayout(font,"Player " + (turn + 1) + ": paid rent for the card " + players[turn].getCellnumber()
                            + ". Pl " + (turn + 1) + " have " + players[turn].getMoney());
                     font.draw(batch, layout, 400, 750);
                    
                    if (!players[turn].isActive()) {
                      // System.out.println("Pl " + (turn + 1) + ": removed from the game");
                        layout = new GlyphLayout(font,"Player " + (turn + 1) + ": removed from the game");
                        font.draw(batch, layout, 400, 750);
                    }
                }
                rentpayed = true;
            }

            if (tapEnter || tapSpace) {
                if (tapSpace && !checkOwner(cells, players, turn) && cells[players[turn].getCellnumber()].getAccessTobuy()) {
                    players[turn].buyCard(cells);
                }
                turnFinished = true;
                rentpayed = false;
                while (true) {
                    if (dice1 != dice2 || !players[turn].isActive()) {
                        turn++;
                    }
                    if (turn >= countPlayers) {
                        turn = 0;
                    }
                    if (players[turn].isActive()) {
                        break;
                    }
                }
            }
        }

        if (isCreated) {
            for (int i = 0; i < countPlayers; i++) {
                if (players[i].isActive()) {
                    batch.draw(players[i].getTexture(), players[i].getX(), players[i].getY());
                }
            }
        }

        if (Player.getActivePlayers() == 1) {
            batch.draw(winImg, 115, 329);
        }

        batch.end();

        if (!isCreated && Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            isCreated = true;
            players = new Player[4];
            players[0] = new Player(cells, 1, 0, new Texture("40x40 abram.png"), new Texture("55x55 abram.png"));
            players[1] = new Player(cells, 2, 1, new Texture("40x40 morti.png"), new Texture("55x55 rick.png"));
            players[2] = new Player(cells, 3, 2, new Texture("40x40 spider.png"), new Texture("55x55 spider.png"));
            players[3] = new Player(cells, 4, 3, new Texture("40x40 bat.png"), new Texture("55x55 bat.png"));
        }

        if (!playersCreated && Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) {
            countPlayers = 2;
            playersCreated = true;
            setActivePlayers(countPlayers);
        }

        if (!playersCreated && Gdx.input.isKeyJustPressed(Input.Keys.NUM_3)) {
            countPlayers = 3;
            playersCreated = true;
            setActivePlayers(countPlayers);
        }

        if (!playersCreated && Gdx.input.isKeyJustPressed(Input.Keys.NUM_4)) {
            countPlayers = 4;
            playersCreated = true;
            setActivePlayers(countPlayers);
        }

        if (playersCreated && turnFinished && isCreated && Gdx.input.isKeyJustPressed(Input.Keys.RIGHT) && Player.getActivePlayers() != 1) {
            dice1 = dice();
            dice2 = dice();
            players[turn].move(dice1 + dice2, cells, turn);
            turnFinished = false;
        }
        //choose players
    }

    @Override
    public void dispose() {
        batch.dispose();
        img.dispose();
        for (int i = 0; i < countPlayers; i++) {
            players[i].getTexture().dispose();
        }
    }

    private void loadCells() {
        boolean accessToBuy;
        try {
            Scanner sc = new Scanner(new File("Cells.txt"));
            for (int i = 0; i < 80; i += 2) {
                accessToBuy = checkCell(i / 2);
                cells[i / 2] = new Cell(
                        Integer.parseInt(sc.nextLine()),
                        Integer.parseInt(sc.nextLine()),
                        Integer.parseInt(sc.nextLine()),
                        Integer.parseInt(sc.nextLine()),
                        accessToBuy);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private int dice() {
        return ((int) (Math.random() * 6) + 1);
    }

    public static boolean checkOwner(Cell[] cells, Player[] players, int turn) {
        int tr = cells[players[turn].getCellnumber()].getOwner();
        return tr != 4 && tr != turn;
    }

    public boolean checkCell(int i) {
        int[] arr = {0, 1, 3, 8, 10, 17, 20, 22, 30, 33, 36};
        for (int j = 0; j < arr.length; j++) {
            if (i == arr[j]) {
                return false;
            }
        }
        return true;
    }
}
