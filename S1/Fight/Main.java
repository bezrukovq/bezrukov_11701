import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
		boolean turn = true;
		int hit;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first player name:");
		String name = sc.nextLine();
		Player p1 = new Player(name);
		System.out.println("Enter second player name:");
		name = sc.nextLine();
		Player p2 = new Player(name);
		while ((p1.getHp())>0 && p2.getHp()>0){
			System.out.println(p1.getName()+"				"+p2.getName());
			System.out.println(p1.getHp()+  "				"+p2.getHp());
			if (turn) {
				System.out.println(p1.getName()+"'s turn");
				hit = sc.nextInt();
				p2.hurt(hit);
			} else {
				System.out.println(p2.getName()+"'s turn");
				hit = sc.nextInt();
				p1.hurt(hit);
			}
			turn = !turn;



		}
	}
}
