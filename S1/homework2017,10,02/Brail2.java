public class Brail2 {
	public static void main(String[] args){
		int[][] bit = new int[3][2*(args.length)];
		int i = 0; 
		int j = 0;
		int b = 0;
		int n;
		int k;
		for (k=0; k<(args.length); k++){
			n = Integer.parseInt(args[k]);
			j = 0;
			i = k*2;
			while (n != 0){
				bit[j][i] = n % 2;
				n = n>>1;
				if ((i+1) % 2 ==0) {
			 		j++;
			 		i = k*2;
			 	} else { i++;}
			}
		}
		for ( i = 0; i<3; i++){
			k = 0;
			for (int v: bit[i]){
				if (v == 1){
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
				if (k % 2 ==1){
					System.out.print("  ");
				}
				k++;
			}
			System.out.println("");
		}
	}
}