// Ряд a

import java.util.Scanner;
public class RyadA {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double a = 1;
		double eps = 0.000000001;
		double sum =1;
		double stx = 1;
		double kp =1;
		int k=1;
		while (Math.abs(a)>eps){
			stx *= x;
			kp = k + 1;
			a = stx/kp;
			sum += a;
			k++;
			System.out.println(sum);
		}

		System.out.println(sum);
	}
}