// Ряд б

import java.util.Scanner;
public class RyadB {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double a = 1;
		double eps = 0.000000001;
		double sum =1;
		double stx = 1;
		double kf =1;
		int k=1;
		while (Math.abs(a)>eps){
			stx *= x*x;
			kf *=2*k*(2*k-1);
			a = stx/kf;
			sum += a;
			k++;
			System.out.println(sum);
		}

		System.out.println(sum);
	}
}