// Ряд в
public class RyadV {
	public static void main(String[] args){
		double x = Double.parseDouble(args[0]);
		double a = 1;
		double eps = 0.000001;
		double sum =1;
		double stx = 1;
		double kff;
		int k=1;
		double stt = 1;
		while (Math.abs(a)>eps){
			kff = kfff(k);
			stt *= 2;
			stx *= x*x;
			a = (stx*stt)/kff;
			sum += a;
			k++;
			System.out.println(sum);
		}
	}
	static double kfff(int k){
		double kf;
		if ( k == 1){
			return 1;
		}
		if ( k == 2){
			return 2;
		}
		if ( k == 3){
			return 3;
		}
		if (k % 2 == 0){
			kf = 2;
		} else {
			kf = 3;
		}
		for (int i = (int)kf + 2; 2*i<=k; i += 2){
			kf *= i;
		}
		return kf;
	}
}