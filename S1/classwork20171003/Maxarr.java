public class Maxarr {
	public static void main(String[] args) {
		int max = Integer.parseInt(args[0]);
		int s;
		for (String n: args){
			s = Integer.parseInt(n);
			 max = max < s ? s : max;
		}
		System.out.println(max);
	}
}