import java.util.Scanner;

public class Trey0 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int l = sc.nextInt();
        double[][] mat = new double[l][l];
        for (int j = 0; j < l; j++) {
            for (int i = 0; i < l; i++) {
                mat[j][i] = sc.nextInt();
            }
        }

        for (int k = 0; k < l - 1; k++) {
            for (int j = k + 1; j < l; j++) {
                for (int i = k + 1; i < l; i++) {
                    if (mat[k][k] != 0) {
                        mat[j][i] = mat[k][k] * mat[j][i] - mat[k][i] * mat[j][k];
                    }
                }
                mat[j][k] = 0;
            }
            for (int i = 0; i < l; i++) {
                for (double n : mat[i]) {
                    System.out.print(n);
                    System.out.print("  ");
                }
                System.out.println(" ");
            }
            System.out.println(" ");
        }
    }
}