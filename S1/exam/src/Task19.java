import java.util.Scanner;

public class Task19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        int sumStr = 0;
        String[] arr = new String[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextLine();
        }
        for (int i = 0; i < n && sumStr < 2; i++) {
            sumStr = check(arr[i], 'q', 3) || check(arr[i], 'z', 5) ? ++sumStr : sumStr;
        }
        System.out.println(sumStr == 2);
    }

    public static boolean check(String a, char b, int c) {
        int sumB = 0;
        for (int i = 0; i < a.length() && sumB < c + 1; i++) {
            sumB = a.charAt(i) == b ? ++sumB : sumB;
        }
        return (sumB != c + 1);
    }

}
