//Task25
public class Money {
    private long rubbles;
    private byte pennies;

    public Money(long rubbles, int pennies) {
        this.rubbles = rubbles;
        this.pennies = reduce(pennies);
    }

    public String toString() {
        return rubbles + "," + pennies;
    }

    public byte reduce(int pennies) {
        if (pennies > 99) {
            rubbles += pennies / 100;
            pennies %= 100;
        } else if (pennies < 0) {
            rubbles += pennies / 100;
            pennies = 100 + pennies % 100;
        }
        return (byte) pennies;
    }

    public static Money sum(Money m1, Money m2) {
        return new Money(m1.getRubbles() + m2.getRubbles(), m1.getPennies() + m2.getPennies());

    }

    public static Money sub(Money m1, Money m2) {
        return new Money(m1.getRubbles() - m2.getRubbles(), m1.getPennies() - m2.getPennies());

    }

    public void mult(int a) {
        rubbles = rubbles * a;
        pennies = reduce(pennies * a);
    }

    public static byte compare(Money m1, Money m2) {
        if (m1.getRubbles() > m2.getRubbles() || m1.getRubbles() == m2.getRubbles() && m1.getPennies() > m2.getPennies()) {
            System.out.println("Первая сумма больше");
            return 1;
        } else if (m2.getRubbles() > m1.getRubbles() || m2.getRubbles() == m1.getRubbles() && m2.getPennies() > m1.getPennies()) {
            System.out.println("Вторая сумма больше");
            return -1;
        }
        System.out.println("Суммы равны");
        return 0;
    }

    public long getRubbles() {
        return rubbles;
    }

    public byte getPennies() {
        return pennies;
    }


    public static void main(String[] args) {
        Money m1 = new Money(123, 12);
        Money m2 = new Money(1, 125);
        Money m3 = Money.sum(m1, m2);
        System.out.println(m1);
        System.out.println(m2);
        System.out.println("m1+m2=" + m3);
        m3 = Money.sub(m1, m2);
        System.out.println("m1-m2=" + m3);
        m3.mult(2);
        System.out.println("previous*2=" + m3);
        System.out.println("сравнение m1 с m2");
        int a = Money.compare(m1, m2);
        System.out.println(a);
    }
}
