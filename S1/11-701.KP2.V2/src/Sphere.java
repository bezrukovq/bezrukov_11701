public class Sphere implements Volumeable {
    private String name = "Sphere";
    private double R;

    public Sphere(int R) {
        this.R = R;

    }

    public double getV() {
        return (4 / 3) * Math.PI * R * R * R;
    }

    public double getS() {
        return 4 * Math.PI * R * R;
    }

    public String getName() {
        return name;
    }
}
