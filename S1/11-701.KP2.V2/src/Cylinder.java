public class Cylinder implements Volumeable {
    private int H;
    private int R;
    private String name = "Cylinder";

    public Cylinder(int h, int r) {
        H = h;
        R = r;
    }

    public double getV() {
        return Math.PI * R * R * H;
    }

    public double getS() {
        return 2 * Math.PI * R * (H + R);
    }

    public String getName() {
        return name;
    }
}
