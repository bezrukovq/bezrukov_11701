/**
 * @author Vladimir Bezrukov
 * 11-701
 * KP2 Var 2
 */

public class Main {
    public static void main(String[] args) {
        Volumeable[] figures = new Volumeable[4];
        figures[0] = new Sphere(3);
        figures[1] = new Cylinder(3, 5);
        figures[2] = new Sphere(5);
        figures[3] = new Cylinder(1, 1);
        for (Volumeable n: figures) {
            System.out.println(n.getName());
            System.out.println("Объем   =  " + n.getV());
            System.out.println("Площадь =  " + n.getS());
        }
    }
}
