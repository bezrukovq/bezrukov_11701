public interface Volumeable {

    double getS();

    default double getV() {
        return 0;
    }

    default String getName() {
        return "figure";
    }

}
