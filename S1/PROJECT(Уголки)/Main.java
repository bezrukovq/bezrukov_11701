import java.util.Scanner;

public class Main {

    public static  Scanner in = new Scanner(System.in);
    public static boolean flag = false;
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void main(String[] args) {
        System.out.println("Правила игры в уголки");
        System.out.println("Надо поменяться местами. Выигрывает тот, кто первый полностью займёт территорию вражеского ГОРОДА своими ПЕШКАМи.");
        System.out.println("Ходить можно по горизонтали и по вертикали вперёд и назад в любом направлении на одну клетку.");
        System.out.println("Можно Пешками перепрыгивать через свои и чужие Пешки. Можно перепрыгивать за один ход несколько раз подряд.");
        String exit;
        int x = 0;
        while ( x == 0){
            String[][] array = new String[9][9];
            flag = false;
            Spawn(array);
            System.out.print("");
            System.out.println("Вводить координаты можно только в виде БУКВАЦNФРА, например, A1");
            System.out.println();
            System.out.println(ANSI_BLUE+"Ходят белые"+ANSI_RESET);
            Showdesk(array);
            Continue(array);
            exit = in.nextLine();
            if (exit.equals("exit")){
                onExit();
            }
        }


    }
    public static void Showdesk(String [][]array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if(j==0){
                    System.out.print("");
                }
                if (i == 0 || j ==0){
                    System.out.print(ANSI_YELLOW+array[i][j] + " | "+ANSI_RESET);
                } else System.out.print(array[i][j] + " | ");
            }
            System.out.println();
            System.out.print("");
            System.out.println("___________________________________");
        }
    }
    public static void Spawn(String[][] array) {
        int t = 0;
        int code = 65;
        for (int k = 1; k < 9; k++){
            t++;
            array[0][k] = ""+(char)code + "";
            array[k][0] = "" + t + "";
            code++;
            for (int l = 1; l < 9; l++){
                if (k<4 && l<4)
                    array[k][l] = "○";
                else if (k>5 && l>5)
                    array[k][l] = "●";
                else
                    array[k][l] = " ";
            }
        }
        array[0][0]=" ";
    }
    public static void SpawnFinishDesk(String[][] array) {
        if (!flag){
            System.out.println(ANSI_BLUE+"Ходят белые"+ANSI_RESET);
        } else System.out.println(ANSI_PURPLE+"Ходят черные"+ANSI_RESET);
        int t = 0;
        int code = 65;
        for (int k = 1; k < 9; k++){
            t++;
            array[0][k] = ""+(char)code + "";
            array[k][0] = "" + t + "";
            code++;
            for (int l = 1; l < 9; l++){
                if (k<4 && l<4)
                    array[k][l] = "●";
                else if (k>5 && l>5)
                    array[k][l] = "○";
                else
                    array[k][l] = " ";
            }
        }
        array[0][0]=" ";
        array[6][6]=" ";
        array[3][3]=" ";
        array[6][5]="○";
        array[5][3]="●";
        System.out.println();
        Showdesk(array);
        Continue(array);
    }
    public static int getCoordinateOne(String command,String[][]array){
        int x = 0;
        if (command.length()==2 && command.charAt(0) > 64 && command.charAt(0) < 73) {

            x = command.charAt(0) - 64;
            return x;
        } else if(command.equals("exit")) {
            onExit();
            return 0;
        } else if(command.equals("finish")) {
            SpawnFinishDesk(array);
            return 0;
        } else {
            System.out.println(ANSI_RED+"Неверно введены координаты"+ANSI_RESET);
            return 0;
        }
    }
    public static int getCoordinateTwo(String command){
        int y=0;
        if (command.length()==2 && command.charAt(1) > 48 && command.charAt(1) < 57){
            y = command.charAt(1)-48;

            return y;

        }
        else {
            System.out.println(ANSI_RED+"Неверно введены координаты"+ANSI_RESET);
            return 0;
        }
    }

    public static void Jump2(String [][] array,String cell,int pos2,int pos1){
        Showdesk(array);
        System.out.println(ANSI_YELLOW+"Перепрыгнуть еще раз?"+ANSI_RESET);
        String ask = in.nextLine();
        if (ask.equals("yes")){
            System.out.println("Введите координаты ячейки, куда будет перемещена шашка: ");
            String commandTwo = in.nextLine();
            int posGo1 = getCoordinateOne(commandTwo,array);
            int posGo2 = getCoordinateTwo(commandTwo);
            int i = 0;
            int j = 0;
                if ((array[pos2-1][pos1].equals("○") || array[pos2][pos1-1].equals("○") || array[pos2+1][pos1].equals("○") || array[pos2][pos1+1].equals("○")) && array[posGo2][posGo1].equals(" ") || (
                        (array[pos2-1][pos1].equals("●") || array[pos2][pos1-1].equals("●") || array[pos2+1][pos1].equals("●") || array[pos2][pos1+1].equals("●")) && array[posGo2][posGo1].equals(" "))){
                        array[posGo2][posGo1] = cell;
                        array[pos2][pos1] = " ";
                        Jump2(array,cell,posGo2,posGo1);
                }

                else System.out.println(ANSI_RED+"Шаг 2 клетки или прыжок невозможен"+ANSI_RESET);
        }
    }


    public static void Go(int pos1, int pos2, int posGo1, int posGo2, String [][] array){
        String cell = array[pos2][pos1];
        if (!flag){
            if (cell.equals("●")){
                if(Math.abs(posGo1-pos1) == 1 && Math.abs(posGo2-pos2) == 0 || Math.abs(posGo2-pos2) ==1 && Math.abs(posGo1-pos1) == 0) {
                    if (array[posGo2][posGo1].equals(" ")) {
                        array[posGo2][posGo1] = cell;
                        array[pos2][pos1] = " ";
                        flag = true;
                    } else if (array[posGo2][posGo1].equals("●") || array[posGo2][posGo1].equals("○")) {
                        System.out.println(ANSI_RED+"Тут стоит шашка!"+ANSI_RESET);
                    }
                } else if (Math.abs(posGo1-pos1) == 2 && Math.abs(posGo2-pos2) == 0 || Math.abs(posGo2-pos2) ==2 && Math.abs(posGo1-pos1) == 0) {
                    if ((array[pos2-1][pos1].equals("○") || array[pos2][pos1-1].equals("○") || array[pos2+1][pos1].equals("○") || array[pos2][pos1+1].equals("○")) && array[posGo2][posGo1].equals(" ") ||
                            (array[pos2-1][pos1].equals("●") || array[pos2][pos1-1].equals("●") || array[pos2+1][pos1].equals("●") || array[pos2][pos1+1].equals("●")) && array[posGo2][posGo1].equals(" ")){
                        array[posGo2][posGo1] = cell;
                        array[pos2][pos1] = " ";
                        Jump2(array, cell, posGo2, posGo1);
                        flag = true;
                    } else System.out.println(ANSI_RED+"Шаг 2 клетки или прыжок невозможен"+ANSI_RESET);

                } else System.out.println(ANSI_RED+"Слишком большой шаг"+ANSI_RESET);
            }
        } else {
            if (cell.equals("○")){
                if(Math.abs(posGo1-pos1) == 1 && Math.abs(posGo2-pos2) == 0 || Math.abs(posGo2-pos2) ==1 && Math.abs(posGo1-pos1) == 0) {
                    if (array[posGo2][posGo1].equals(" ")) {
                        array[posGo2][posGo1] = cell;
                        array[pos2][pos1] = " ";
                        flag = false;
                    } else if (array[posGo2][posGo1].equals("●") || array[posGo2][posGo1].equals("○")) {
                        System.out.println(ANSI_RED+"Тут стоит шашка!"+ANSI_RESET);
                    }
                } else if (Math.abs(posGo1-pos1) == 2 && Math.abs(posGo2-pos2) == 0 || Math.abs(posGo2-pos2) ==2 && Math.abs(posGo1-pos1) == 0) {
                    if ((array[pos2-1][pos1].equals("○") || array[pos2][pos1-1].equals("○") || array[pos2+1][pos1].equals("○") || array[pos2][pos1+1].equals("○")) && array[posGo2][posGo1].equals(" ") ||
                            (array[pos2-1][pos1].equals("●") || array[pos2][pos1-1].equals("●") || array[pos2+1][pos1].equals("●") || array[pos2][pos1+1].equals("●")) && array[posGo2][posGo1].equals(" ")){
                        array[posGo2][posGo1] = cell;
                        array[pos2][pos1] = " ";
                        Jump2(array,cell,posGo2,posGo1);
                        flag = false;
                    } else System.out.println(ANSI_RED+"Шаг 2 клетки или прыжок невозможен"+ANSI_RESET);

                } else System.out.println(ANSI_RED+"Слишком большой шаг"+ANSI_RESET);
            }
        }



    }
    public static void Continue(String[][]array){
        String exit;
        byte ch = 0;
        int b = 0;
        String kl;
        System.out.println("Введите координаты шашки, которая будет участвовать в ходе: ");
        String commandOne = in.nextLine();
        int a = getCoordinateOne(commandOne,array);
        if (a != 0) {
            b = getCoordinateTwo(commandOne);}
        if (!flag)
            kl = "●";
        else kl = "○";
        if (a !=0 && b != 0 && (array[b][a]==kl)){
            System.out.println("Введите координаты ячейки, куда будет перемещена шашка: ");
            String commandTwo = in.nextLine();
            int c = getCoordinateOne(commandTwo,array);
            int d = getCoordinateTwo(commandTwo);
            if (c != 0 && d!= 0){
                Go(a, b, c, d, array);}
        } else {
            if (array[b][a].equals(" ")) {
                System.out.println("Клетка пустая!");
            } else if (array[b][a].equals("●") && flag){
                System.out.println("Ходят черные!!!");
            }else if (array[b][a].equals("○") && !flag){
                System.out.println("Ходят белые!!!");
            }

        }
        System.out.println();
        if (!flag){
            System.out.println(ANSI_BLUE+"Ходят белые"+ANSI_RESET);
        } else System.out.println(ANSI_PURPLE+"Ходят черные"+ANSI_RESET);
        Showdesk(array);

        ch = Check(array);
        if (ch==0)
            Continue(array);
        else {
            if (ch == 1){
                System.out.println("Белые победили!");
            } else {
                System.out.println("Черные победили!");
            }
        }
    }
    public static byte Check(String[][]array){
        boolean checkw = true;
        boolean checkb = true;
        for (int j = 1; j < 9; j++) {
            for (int i = 1; i < 9; i++) {
                if (j>5 && i>5)
                    checkb = (checkb)&&(array[j][i].equals("○"));
                else if (j<4 && i<4)
                    checkw = (checkw)&&(array[j][i].equals("●"));
            }
        }
        if (checkw==true)
            return 1;
        else if (checkb==true)
            return 2;
        else return 0;
    }
    public static void onExit(){

        System.exit(0);
    }
}