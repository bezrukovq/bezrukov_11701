import java.util.Scanner;

public class Task3 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        int max = 0;
        for (int i = 0; i<n; i++) {
            arr[i] = sc.nextInt();
            if (arr[i]>max){
                max = arr[i];
            }
        }
        max--;
        for (int i = max; i>=0; i--){
            for (int a: arr){
                if (a>i)
                    System.out.print("0");
                 else
                    System.out.print(" ");
                 System.out.print("  ");
            }
            System.out.println("");
        }
    }
}
