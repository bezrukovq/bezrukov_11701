import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int mod;
        int elemWithSolution = 0;
        int sumDigitsOfElems = 0;
        boolean sumOfElemsOnlyNech = true;
        int st2 = 1;
        int iFactorial = 1;
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();

            if (i != 0) {
                st2 *= 2;
            }
            if (i != 0) {
                iFactorial *= i;
            }
            if (iFactorial * iFactorial - 4 * (factorial(arr[i]) - st2) >= 0) {
                elemWithSolution++;

            }
        }

        if (elemWithSolution != 0) {
            while (elemWithSolution > 1) {
                sumDigitsOfElems += elemWithSolution % 10;
                elemWithSolution /= 10;

            }
            while (sumDigitsOfElems > 1 && sumOfElemsOnlyNech) {
                mod = sumDigitsOfElems % 10;
                if (mod % 2 != 0) {
                    sumOfElemsOnlyNech = false;
                }
                sumDigitsOfElems /= 10;

            }
        } else
            sumOfElemsOnlyNech = false;


        System.out.println("2.Только нечетные цифры в кол-ве элементов при которых уравнение имеет решение - " + sumOfElemsOnlyNech);

    }
    public static int factorial(int i){
        int factorial = 1;
        for ( int j =2; j<=i;j++){
            factorial *= j;
        }
        return factorial;
    }
}
