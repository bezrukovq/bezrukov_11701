package Task08PS3;
/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 3 Task 08
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class MAin {
    private static ArrayList<String> sites = new ArrayList<>();
    private static ArrayList<Double> GChrome = new ArrayList<>();
    private static ArrayList<Double> IE = new ArrayList<>();
    private static ArrayList<Double> Firefox = new ArrayList<>();
    private static ArrayList<Double> Safari = new ArrayList<>();
    static ArrayList<WebSites> webSites = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException {
        readFile();
        createWebSites();
        System.out.println(WebSites.schnelleSite().getName());
        System.out.println(WebSites.fastestBrowser());
        System.out.println(WebSites.totalTime());
    }


    private static void createWebSites() {
        for (int i = 0; i < sites.size(); i++) {
            webSites.add(new WebSites(sites.get(i), GChrome.get(i), IE.get(i), Firefox.get(i), Safari.get(i)));
        }
    }

    private static void readFile() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("src/Task08PS3/Browser_Speed.csv"));
        String text = sc.nextLine();
        text = text.replaceAll("\\.", "");
        String[] textArr = text.split(",");

        for (int i = 0; i < textArr.length - 3; i++) {
            sites.add(textArr[i + 3]);
        }

        sc.nextLine();
        text = sc.nextLine();
        textArr = text.split(",");
        for (int i = 0; i < textArr.length - 3; i++) {
            GChrome.add(Double.valueOf(textArr[i + 3]));
        }

        text = sc.nextLine();
        textArr = text.split(",");
        for (int i = 0; i < textArr.length - 3; i++) {
            IE.add(Double.valueOf(textArr[i + 3]));
        }

        text = sc.nextLine();
        textArr = text.split(",");
        for (int i = 0; i < textArr.length - 3; i++) {
            Firefox.add(Double.valueOf(textArr[i + 3]));
        }

        text = sc.nextLine();
        textArr = text.split(",");
        for (int i = 0; i < textArr.length - 3; i++) {
            Safari.add(Double.valueOf(textArr[i + 3]));
        }

    }

}
