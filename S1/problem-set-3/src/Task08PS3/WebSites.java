package Task08PS3;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 8
 */

public class WebSites {
    private String name;

    private double GChrome;
    private double IE;
    private double Firefox;
    private double Safari;

    public String getName() {
        return name;
    }

    public double getGChrome() {
        return GChrome;
    }

    public double getIE() {
        return IE;
    }

    public double getFirefox() {
        return Firefox;
    }

    public double getSafari() {
        return Safari;
    }


    public WebSites(String name, double GChrome, double IE, double Firefox, double Safari) {
        this.name = name;
        this.GChrome = GChrome;
        this.IE = IE;
        this.Firefox = Firefox;
        this.Safari = Safari;
    }


    public static WebSites schnelleSite() {
        WebSites fastestSite = null;
        double min = Double.MAX_VALUE;
        for (WebSites site : MAin.webSites) {
            double sum = 0;
            for (int i = 0; i < 4; i++) {
                sum = site.getGChrome() + site.getFirefox() + site.getIE() + site.getSafari();
                sum /= 4;
            }
            if (sum < min) {
                min = sum;
                fastestSite = site;
            }
        }
        return fastestSite;
    }

    public static String fastestBrowser() {
        WebSites fastestSite = null;
        double min = Double.MAX_VALUE;
        double GChrome = 0;
        double IE = 0;
        double Firefox = 0;
        double Safari = 0;
        for (WebSites site : MAin.webSites) {
            GChrome += site.getGChrome();
            IE += site.getIE();
            Firefox += site.getFirefox();
            Safari += site.getSafari();
        }
        if (GChrome < IE && GChrome < Firefox && GChrome < Safari) {
            return "GChrome";
        }
        if (IE < GChrome && IE < Firefox && IE < Safari) {
            return "IE";
        }
        if (Firefox < IE && Firefox < GChrome && Firefox < Safari) {
            return "Firefox";
        } else {
            return "Safari";
        }
    }

    public static double totalTime() {
        double sum = 0;
        for (WebSites site : MAin.webSites) {
            sum += site.getGChrome() + site.getFirefox() + site.getIE() + site.getSafari();
        }
        return sum /= 36;
    }
}