package Task02PS3;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 02
 */

public class Player {
    private String name;
    private int hp;

    public Player(String name) {
        hp = 100;
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public String getName() {
        return name;
    }

    public void hurt(int hit) {
        if (((int) (Math.random() * 9) + 1) < hit) {
            System.out.println("Мимо!");
            hit = 0;
        } else {
            System.out.println(name + " ранен");
        }
        this.hp -= hit;
        if (hp < 1) {
            System.out.println(name + " убит");
        }

    }
}
