package Task02PS3;

import java.util.Scanner;


/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 02
 */

public class Game {
    boolean turn = true;
    int hit;
    Scanner sc = new Scanner(System.in);

    public Game() {
        System.out.println("Enter first player name:");
        String name = sc.nextLine();
        Player p1 = new Player(name);
        System.out.println("Enter second player name:");
        name = sc.nextLine();
        Player p2 = new Player(name);
        Play(p1, p2);
    }

    public void Play(Player p1, Player p2) {
        while ((p1.getHp()) > 0 && p2.getHp() > 0) {
            System.out.println(p1.getName() + "				" + p2.getName());
            System.out.println(p1.getHp() + "				" + p2.getHp());
            if (turn) {
                System.out.println(p1.getName() + "'s turn");
                hit = sc.nextInt();
                p2.hurt(hit);
            } else {
                System.out.println(p2.getName() + "'s turn");
                hit = sc.nextInt();
                p1.hurt(hit);
            }
            turn = !turn;
        }
    }
}
