package Task9_10PS3;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 3 Task 9-10
 */

public class Database {
    final static int numberOfUsers = 4;
    final static int numberOfMessages = 4;
    final static int numberOfSubs = 2;
    static User[] users = new User[numberOfUsers];
    static Message[] messages = new Message[numberOfMessages];
    static Subscriptions[] subs = new Subscriptions[numberOfSubs];

    public static User[] loadUsers() throws FileNotFoundException {

        Scanner scanner = new Scanner(new File("src/Task9_10PS3/users.txt"));
        for (int i = 0; i < numberOfUsers; i++) {
            long id = Long.parseLong(scanner.nextLine());
            String username = scanner.nextLine();
            String password = scanner.nextLine();
            String genderS = scanner.nextLine();
            User.Gender gender = User.Gender.valueOf(genderS);
            String email = scanner.nextLine();
            users[i] = new User(id, username, password, gender, email);
        }
        return users;
    }

    public static Message[] loadMessages() throws FileNotFoundException, ParseException {


        Scanner scanner = new Scanner(new File("src/Task9_10PS3/messages.txt"));

        for (int i = 0; i < numberOfMessages; i++) {
            long sender_id = Long.parseLong(scanner.nextLine());
            long receiver_id = Long.parseLong(scanner.nextLine());
            String dateString = scanner.nextLine();
            String text = scanner.nextLine();
            Message.Status status = Message.Status.valueOf(scanner.nextLine());

            messages[i] = new Message();
            for (User user : users) {
                if (user.getId() == sender_id) {
                    messages[i].setSender(user);
                    break;
                }
            }
            for (User user : users) {
                if (user.getId() == receiver_id) {
                    messages[i].setReceiver(user);
                    break;
                }
            }
            messages[i].setText(text);
            DateFormat df = new SimpleDateFormat("dd-mm-yyyy, HH:mm:ss");
            Date date = df.parse(dateString);
            messages[i].setDate(date);
            messages[i].setStatus(status);
        }
        return messages;
    }

    public static Subscriptions[] loadSubs() throws FileNotFoundException {

        Scanner scanner = new Scanner(new File("src/Task9_10PS3/subscriptions.txt"));
        for (int i = 0; i < numberOfSubs; i++) {
            long subscription_id = Long.parseLong(scanner.nextLine());
            long subscriber_id = Long.parseLong(scanner.nextLine());
            subs[i] = new Subscriptions();

            for (User user : users) {
                if (user.getId() == subscription_id) {
                    subs[i].setSubscription(user);
                    break;
                }
            }

            for (User user : users) {
                if (user.getId() == subscriber_id) {
                    subs[i].setSubscriber(user);
                    break;
                }
            }

        }
        return subs;
    }


}