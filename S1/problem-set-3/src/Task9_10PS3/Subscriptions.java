package Task9_10PS3;
/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 9-10
 */
public class Subscriptions {
    private User subscription;
    private User subscriber;

    public void setSubscriber(User subscriber) {
        this.subscriber = subscriber;
    }

    public User getSubscriber() {
        return subscriber;
    }

    public User getSubscription() {
        return subscription;
    }

    public void setSubscription(User subscription) {
        this.subscription = subscription;

    }
}