/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 3 Task 6
 */

public class RationalVector2D {
    private RationalFraction x;
    private RationalFraction y;

    public RationalVector2D() {
        x = new RationalFraction();
        y = new RationalFraction();
    }

    public RationalVector2D(RationalFraction x, RationalFraction y) {
        this.x = x;
        this.y = y;
    }

    public RationalVector2D add(RationalVector2D Vector) {
        RationalVector2D sumVector = new RationalVector2D();
        sumVector.setX(x.add(Vector.getX()));
        sumVector.setY(y.add(Vector.getY()));
        return sumVector;
    }

    public double scalarProduct(RationalVector2D v2) {
        return v2.getY().value() * this.y.value() + v2.getX().value() * this.x.value();
    }

    public String toString() {
        return "x : " + x + " \n y :" + y;
    }

    public double length() {
        return Math.sqrt(x.value() * x.value() + y.value() * y.value());
    }


    public RationalFraction getX() {
        return x;
    }

    public RationalFraction getY() {
        return y;
    }

    public void setX(RationalFraction x) {
        this.x = x;
    }

    public void setY(RationalFraction y) {
        this.y = y;
    }
}

