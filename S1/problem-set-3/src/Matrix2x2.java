/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 3 Task 5
 */

public class Matrix2x2 {
    private double[][] a;

    public Matrix2x2() {
        a = new double[2][2];
        a[0][0] = 0;
        a[0][1] = 0;
        a[1][0] = 0;
        a[1][1] = 0;
    }

    public Matrix2x2(double f) {
        a = new double[2][2];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; i++) {
                a[i][j] = f;
            }
        }
    }

    public Matrix2x2(double a1, double b1, double c1, double d1) {
        a = new double[2][2];
        a[0][0] = a1;
        a[0][1] = b1;
        a[1][0] = c1;
        a[1][1] = d1;
    }

    public Matrix2x2(double[][] b) {
        a = new double[2][2];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; i++) {
                a[i][j] = b[i][j];
            }
        }
    }

    public Matrix2x2 add(Matrix2x2 b) {
        double[][] d = new double[2][2];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; i++) {
                d[i][j] = a[i][j] + b.a[i][j];
            }
        }
        return new Matrix2x2(d);
    }

    public Matrix2x2 sub(Matrix2x2 Matrix) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] -= Matrix.getArr()[i][j];
            }
        }
        return this;
    }

    public Matrix2x2 mult(double a) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] *= a;
            }
        }
        return this;
    }

    public Matrix2x2 mult(Matrix2x2 Matrix) {
        Matrix2x2 MultMatrix = new Matrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    MultMatrix.getArr()[i][j] += this.a[i][k] *= Matrix.getArr()[k][i];
                }
            }
        }
        return MultMatrix;
    }

    public double det() {
        return a[0][0] * a[1][1] - a[0][1] * a[1][0];
    }

    public void transpose() {
        double c = a[0][1];
        a[0][1] = a[1][0];
        a[1][0] = c;
    }

    public Matrix2x2 inverseMatrix() {
        try {
            double det = det();
            if (det == 0) throw new MatrixException("Нет обратной матрицы");
            double c = a[0][0];
            a[0][0] = a[1][1] / det;
            a[1][1] = c / det;
            a[0][1] /= -det;
            a[1][0] /= -det;
        } catch (MatrixException e) {
            System.out.println("попытка найти обратную матрицу для матрицы с det  =0 ");
        }
        return this;
    }

    public Vector2D multVector(Vector2D v) {
        Vector2D vector = new Vector2D();
        vector.setX(a[0][0] * vector.getX() + a[0][1] * vector.getY());
        vector.setY(a[1][0] * vector.getX() + a[1][1] * vector.getY());
        return vector;
    }

    public String toString() {
        return "___________\n" + a[0][0] + "  " + a[0][1] + "\n" + a[1][0] + "  " + a[1][1] + "\n-----------";
    }

    public double[][] getArr() {
        return a;
    }

}
