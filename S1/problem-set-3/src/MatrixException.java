/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 5
 */
public class MatrixException extends Exception {
    public MatrixException(String message) {
        super(message);
    }
}