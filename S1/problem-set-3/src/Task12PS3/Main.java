package Task12PS3;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 3 Task 12
 */
public class Main {
    public static void main(String[] args) throws NotNaturalNumberException {
        Number[] num = new Number[6];
        num[0] = new SimpleLongNumber(1);
        num[1] = new VeryLongNumber(2);
        num[2] = new SimpleLongNumber(3);
        num[3] = new VeryLongNumber(4);
        num[4] = new SimpleLongNumber(5);
        num[5] = new VeryLongNumber(6);
        Number numw = num[0];
        System.out.println(num[2].compareTo(num[5]));
        for (int i = 1; i < num.length; i++) {
            numw.add(num[i]);
        }
        System.out.println(numw.toString());

    }
}
