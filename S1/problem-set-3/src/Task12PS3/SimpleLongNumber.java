package Task12PS3;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 12
 */
public class SimpleLongNumber implements Number {
    private long number;

    public SimpleLongNumber(long number) {
        this.number = number;
    }

    public long getNumber() {
        return number;
    }

    public Number add(Number n) throws NotNaturalNumberException {
        if (n instanceof SimpleLongNumber) {
            SimpleLongNumber sln = (SimpleLongNumber) n;
            this.number += sln.getNumber();
        } else if (n instanceof VeryLongNumber) {
            VeryLongNumber sln = (VeryLongNumber) n;
            int[] a = sln.getNumber();
            long k = 1;
            for (int i = 1; i <= a.length; i++) {
                this.number += a[a.length - i] * k;
                k *= 10;
            }
        }

        return this;
    }

    public void setNumber(long number) {
        this.number = number;
    }


    public Number sub(Number n) throws NotNaturalNumberException {
        if (this.compareTo(n) < 0) throw new NotNaturalNumberException("не натуральное число");
        if (n instanceof SimpleLongNumber) {
            SimpleLongNumber sln = (SimpleLongNumber) n;
            this.number -= sln.getNumber();
        } else if (n instanceof VeryLongNumber) {
            VeryLongNumber sln = (VeryLongNumber) n;
            int[] a = sln.getNumber();
            long k = 1;
            for (int i = 1; i <= a.length; i++) {
                this.number -= a[a.length - i] * k;
                k *= 10;
            }
        }
        return this;
    }

    public int getCountsOfDigits(long number) {
        return String.valueOf(Math.abs(number)).length();
    }

    public int compareTo(Number n) {
        int res = 0;
        int znak = 0;
        String strNum = this.toString();
        if (n instanceof SimpleLongNumber) {
            SimpleLongNumber sln = (SimpleLongNumber) n;
            res = Long.compare(this.number, sln.getNumber());
        } else if (n instanceof VeryLongNumber) {
            VeryLongNumber sln = (VeryLongNumber) n;
            int[] a = sln.getNumber();
            for (int i = 0; i < a.length; i++) {
                if (a[i] != 0) {
                    znak = a.length - i;
                    break;
                }
            }
            if (znak > getCountsOfDigits(this.number)) {
                res = -1;
            } else if ((znak < getCountsOfDigits(this.number))) {
                res = 1;
            } else {
                if (this.toString().equals(sln.toString())) {
                    res = 0;
                } else {
                    int j = 0;
                    int kk;
                    for (int i = a.length - znak; i < a.length; i++, j++) {
                        kk = Character.getNumericValue(strNum.charAt(j));
                        if (a[i] != kk) {
                            if (a[i] > kk) {
                                res = -1;
                            } else {
                                res = 1;
                            }
                        }
                    }

                }
            }
        }
        return res;
    }

    public String toString() {
        return "" + number;
    }
}
