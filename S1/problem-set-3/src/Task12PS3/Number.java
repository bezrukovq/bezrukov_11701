package Task12PS3;
/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 12
 */
public interface Number {
    Number add(Number n) throws NotNaturalNumberException;
    Number sub(Number n) throws NotNaturalNumberException;
    int compareTo(Number n);
    String toString();
}
