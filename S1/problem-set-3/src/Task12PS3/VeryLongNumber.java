package Task12PS3;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 12
 */
public class VeryLongNumber implements Number {
    private int[] number = new int[20];

    public VeryLongNumber(long number) {
        for (int i = 1; i <= this.number.length; i++) {
            this.number[this.number.length - i] = (int) number % 10;
            number /= 10;
        }
    }


    public Number add(Number n) {
        if (n instanceof SimpleLongNumber) {
            SimpleLongNumber sln = (SimpleLongNumber) n;
            long a = sln.getNumber();
            for (int i = 1; i <= this.number.length; i++) {
                this.number[this.number.length - i] += a % 10;
                sln.setNumber(a /= 10);
            }
        } else if (n instanceof VeryLongNumber) {
            VeryLongNumber sln = (VeryLongNumber) n;
            int[] a;
            a = sln.getNumber();
            for (int i = 0; i < this.number.length && a[i] != 0; i++) {
                this.number[i] += a[i];
            }
        }
        check();
        return this;
    }

    public void check() {
        for (int i = 1; i <= this.number.length; i++) {
            if (this.number[this.number.length - i] > 10) {
                this.number[this.number.length - i - 1] += this.number[this.number.length - i] / 10;
                this.number[this.number.length - i] %= 10;
            }
        }
    }

    public int[] getNumber() {
        return number;
    }

    public void setNumber(int[] number) {
        this.number = number;
    }

    public void checksub() throws NotNaturalNumberException {
        for (int i = 1; i < this.number.length; i++) {
            if (this.number[this.number.length - i] < 0) {
                this.number[this.number.length - i - 1] -= 1;
                this.number[this.number.length - i] += 10;
            }
        }

    }

    public Number sub(Number n) throws NotNaturalNumberException {
        if (this.compareTo(n) < 0) throw new NotNaturalNumberException("не натуральное число");
        if (n instanceof SimpleLongNumber) {
            SimpleLongNumber sln = (SimpleLongNumber) n;
            long a = sln.getNumber();
            for (int i = 1; i <= this.number.length && a != 0; i++) {
                this.number[this.number.length - i] -= (int) a % 10;
                a /= 10;
            }

        } else if (n instanceof VeryLongNumber) {
            VeryLongNumber sln = (VeryLongNumber) n;
            int[] a;
            a = sln.getNumber();
            for (int i = 0; i < this.number.length; i++) {
                this.number[i] -= a[i];
            }
        }
        checksub();
        return this;
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < number.length; i++) {
            if (number[i] != 0) {
                for (int j = i; j < number.length; j++) {
                    result += number[j];
                }
                break;
            }
        }
        return result;
    }

    public int compareTo(Number n) {
        int res = 0;
        int znak = 0;
        for (int i = 0; i < this.number.length; i++) {
            if (this.number[i] != 0) {
                znak = this.number.length - i;
                break;
            }
        }
        String strNum = this.toString();
        if (n instanceof VeryLongNumber) {
            VeryLongNumber sln = (VeryLongNumber) n;
            int[] a = sln.getNumber();
            if (this.toString().equals(sln.toString())) {
                res = 0;
            } else {
                for (int i = 0; i < a.length; i++) {
                    if (this.number[i] != a[i]) {
                        if (this.number[i] > a[i]) {
                            res = 1;
                        } else {
                            res = -1;
                        }
                        break;
                    }
                }
            }
        } else if (n instanceof SimpleLongNumber) {
            SimpleLongNumber sln = (SimpleLongNumber) n;
            if (znak > getCountsOfDigits(sln.getNumber())) {
                res = -1;
            } else if ((znak < getCountsOfDigits(sln.getNumber()))) {
                res = 1;
            } else {
                if (this.toString().equals(sln.toString())) {
                    res = 0;
                } else {
                    int j = 0;
                    int kk;
                    for (int i = this.number.length - znak; i < this.number.length; i++, j++) {
                        kk = Character.getNumericValue(strNum.charAt(j));
                        if (this.number[i] != kk) {
                            if (this.number[i] > kk) {
                                res = -1;
                            } else {
                                res = 1;
                            }
                        }
                    }

                }
            }
        }
        return res;
    }

    public int getCountsOfDigits(long number) {
        return String.valueOf(Math.abs(number)).length();
    }
}
