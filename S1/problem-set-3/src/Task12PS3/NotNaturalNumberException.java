package Task12PS3;
/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 12
 */
public class NotNaturalNumberException extends Exception {
    private int number;

    public int getNumber() {
        return number;
    }

    public NotNaturalNumberException(String message) {
        super(message);
    }
}