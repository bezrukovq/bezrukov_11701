package Task11PS3;

import java.io.IOException;
import java.util.Scanner;

/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 3 Task 11
 */
public class Game {


    public void play() {

        boolean playing = true;
        Player[] players = new Player[2];
        players[0] = new Human("Dart");
        players[1] = new Computer("R2D2");


        //генерируется слово
        String word = "space";
        StringBuffer word_mask = new StringBuffer("");
        for (int i = 0; i < word.length(); i++) {
            word_mask.append("*");
        }
        Scanner sc = new Scanner(System.in);
        int status = 0;


        while (playing) {
            System.out.println("NEW STEP");
            System.out.println("WORD: " + word_mask);
            players[status].makeTurn(word, word_mask);
            if (!word_mask.toString().equals(word)) {
                status = status == 1 ? 0 : 1;
                System.out.println("Wrong, now " + players[status].getName() + "  goes.");
            } else {
                System.out.println(players[status].getName() + " Won");
                playing = false;
            }

        }

    }

    public static void main(String[] args) throws IOException {

        (new Game()).play();
    }
}