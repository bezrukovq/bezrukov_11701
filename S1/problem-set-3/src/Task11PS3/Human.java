package Task11PS3;
/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 11
 */
import java.util.Scanner;

public class Human extends Player {
    public Human(String name){
        super(name);
    }
    public void makeTurn(String word, StringBuffer word_mask){
        Scanner sc = new Scanner(System.in);
        boolean right;
        do {
            right = false;
            char c = sc.nextLine().charAt(0);
            if (word.contains(c + "")) {
                System.out.println("Correct");
                right = true;
                for (int i = 0; i < word.length(); i++) {
                    if (word.charAt(i) == c) {
                        word_mask.setCharAt(i, c);
                    }
                }
                System.out.println("WORD: " + word_mask);
            }
        } while(right && !word.equals(word_mask.toString()));
    }

}
