/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 3 Task 3
 */

public class Vector2D {
    private double x;
    private double y;

    public Vector2D() {
        x = 0;
        y = 0;
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D add(Vector2D Vector) {
        Vector2D sumVector = new Vector2D();
        sumVector.setX(this.x + Vector.getX());
        sumVector.setY(this.y + Vector.getY());
        return sumVector;
    }

    public Vector2D sub(Vector2D Vector) {
        Vector2D subVector = new Vector2D();
        subVector.setX(this.x - Vector.getX());
        subVector.setY(this.y - Vector.getY());
        return subVector;
    }

    public Vector2D mult(double коэффицент_уможения) {
        Vector2D multVector = new Vector2D();
        multVector.setX(this.x * коэффицент_уможения);
        multVector.setY(this.y * коэффицент_уможения);
        return multVector;
    }

    public double scalarProduct(Vector2D v2) {
        return v2.getY() * this.y + v2.getX() * this.x;
    }

    public String toString() {
        return "x : " + x + " \n y :" + y;
    }

    public double length() {
        return Math.sqrt(x * x + y * y);
    }


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}
