/**
 * @author Vladimir Bezrukov
 * 11-701
 * Problem Set 3 Task 7
 */
public class RationalMatrix2x2 {
    private RationalFraction[][] rMat = new RationalFraction[2][2];

    public RationalMatrix2x2() {
        rMat[0][0] = new RationalFraction();
        rMat[0][1] = new RationalFraction();
        rMat[1][0] = new RationalFraction();
        rMat[1][1] = new RationalFraction();

    }

    public RationalMatrix2x2(RationalFraction r) {
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++) {
                rMat[i][j] = new RationalFraction(r.getChis(), r.getZnam());
            }
    }


    public RationalMatrix2x2 add(RationalMatrix2x2 rm, RationalMatrix2x2 rm2) {
        RationalMatrix2x2 rmN = new RationalMatrix2x2();
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++) {
                rmN.rMat[i][j].setChis(rm.rMat[i][j].getChis() + rm2.rMat[i][j].getChis());
                rmN.rMat[i][j].setZnam(rm.rMat[i][j].getZnam() + rm2.rMat[i][j].getZnam());

            }
        return rmN;
    }


    static RationalMatrix2x2 mult(RationalMatrix2x2 a, RationalMatrix2x2 b) {
        int m = a.rMat.length;
        RationalMatrix2x2 c = new RationalMatrix2x2();
        for (int i = 0; i < m; i++)
            for (int j = 0; j < m; j++)
                for (int k = 0; k < m; k++) {

                    c.rMat[i][j].setChis(a.rMat[i][k].getChis() * b.rMat[k][j].getChis());
                    c.rMat[i][j].setZnam(a.rMat[i][k].getZnam() * b.rMat[k][j].getZnam());

                }

        return c;
    }


    public RationalVector2D multVector(RationalVector2D a) {
        RationalFraction rf1 = new RationalFraction();
        RationalFraction rf2 = new RationalFraction();

        rf1.setChis(rMat[0][0].getChis() * a.getX().getChis() / rMat[0][1].getChis() * a.getX().getChis());
        rf1.setZnam(rMat[0][0].getZnam() * a.getX().getZnam() / rMat[0][1].getZnam() * a.getX().getZnam());

        rf2.setChis(rMat[1][0].getChis() * a.getY().getChis() / rMat[1][1].getChis() * a.getY().getChis());
        rf2.setZnam(rMat[1][0].getZnam() * a.getY().getZnam() / rMat[1][1].getZnam() * a.getY().getZnam());

        return new RationalVector2D(rf1, rf2);


    }


}