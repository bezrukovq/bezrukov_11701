
public class Address {
	private String city;
	private String street;
	private String house;
	private int room;

	public void setCity(String city) {
		this.city = city;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public void setRoom(int room) {
		this.room = room;
	}
}