import java.util.Date;
import java.io.*;
public class Patient {
	private String fio;
	private String dateOfBirth;
	private String address;
	private String passportinfo;
	private String gender;
	private int nAnamnesisNotes = 0;
	private String [] anamnesis = new String[nAnamnesisNotes];

	public String getFio(){
		return fio;
	}
	public void setFio(String fio){
		this.fio = fio;
	}
	public Patient(String fio,String dateOfBirth, String address,String passportinfo,String gender,int nAnamnesisNotes, String[] anamnesis){
		this.fio = fio;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
		this.passportinfo = passportinfo;
		this.gender = gender;
		this.nAnamnesisNotes = nAnamnesisNotes;
		this.anamnesis = new String[nAnamnesisNotes];
		for (int i = 0; i < nAnamnesisNotes; i++) {
			this.anamnesis[i] = anamnesis[i];
		}
	}
	public Patient(String fio){
		setFio(fio);
		System.out.println("Patient " + fio + " created");
	}
	public String toString(){
		return "{patient: {name: " + fio + "}}";
	}

	public void dumpToWriter(PrintWriter pw){
		pw.println(fio);
		pw.println(dateOfBirth==null ? "" : dateOfBirth);
		pw.println(address==null ? "" : address);
		pw.println(passportinfo==null ? "" : passportinfo);
		pw.println(gender==null ? "" : gender);
		pw.println(nAnamnesisNotes);
		for (int i = 0; i < nAnamnesisNotes; i++){
			pw.println(anamnesis[i]);
		}
	}
}