public class Hospital {
	private String title;
	private Address address;
	private final int CAPACITY;
	private int numberOfPatients;
	private Patient []patients;
	public Hospital(String title, String city,String street,String house){
		this.title = title;
		this.address = new Address();
		address.setCity(city);
		address.setStreet(street);
		address.setHouse(house);
		this.CAPACITY = 200;
		patients = new Patient[CAPACITY];
		numberOfPatients = 0;
		if (!title.equals("0")){
		System.out.println("Hospital"+ title + "created");
		}
	}
	public void addPatient(Patient p1){
		patients[numberOfPatients] = p1;
		numberOfPatients++;
		System.out.println("Patient added to the hospital");
	}
	public Patient [] getPatients(){
		Patient [] realPatients = new Patient[numberOfPatients];
		for (int i = 0; i < numberOfPatients; i++){
			realPatients[i] = patients[i];
		}
		return realPatients;
	}
}