import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Arrays;
import java.io.*;
public class App {
    private Hospital hospital = new Hospital("0", "0", "0", "0");

    public static void main(String[] args) {
        App app = new App();
        app.init();
        app.start();
        app.save();
    }

    public void start() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            String cmnd = sc.nextLine();
            if (cmnd.equals("exit")) {
                break;
            } else if (cmnd.equals("new p")) {
                System.out.println("Enter name of Patient");
                String name = sc.nextLine();
                Patient p1 = new Patient(name);
                hospital.addPatient(p1);
            } else if (cmnd.equals("show all p")) {
                System.out.println(Arrays.toString(hospital.getPatients()));
            }
        }
    }

    public void save() {
        try {
            PrintWriter pw = new PrintWriter("patients.txt");
            Patient[] patients = hospital.getPatients();
            for (Patient patient : patients) {
                patient.dumpToWriter(pw);
            }
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    public void init() {
        hospital = loadHospital();
        loadPatients();
    }

    public Hospital loadHospital() {
        Scanner sc = null;
        try {
            sc = new Scanner(new File("hospital.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String line = sc.nextLine();
        String[] string = line.split(" ");
        Hospital loadHospital = new Hospital(string[0], string[1], string[2], string[3]);
        return loadHospital;
    }

    public void loadPatients() {
        int j;
        int v = 0;
        String haha;
        int num = 0;
        try {
            Scanner sc = new Scanner(new File("patients.txt"));
            while (sc.hasNextLine()) {
                haha = sc.nextLine();
                num++;
            }
        } catch (FileNotFoundException e) {
        }
        String[] string = new String[num];
        try {
            Scanner sc = new Scanner(new File("patients.txt"));
            while (sc.hasNextLine()) {
                string[v]=sc.nextLine();
                v++;
                num++;
            }
        }
        catch (FileNotFoundException e) {
        }
        for (int i = 0; i<string.length-1; i+=0) {
            j = 0;
            int nAnamnesisNotes = Integer.parseInt(string[i+5]);
            String[] anamnesis = new String[nAnamnesisNotes];
            for (int k = i+5; k<i+5+nAnamnesisNotes; k+=0){
                anamnesis[j] = string[k];
                j++;
            }
            hospital.addPatient(new Patient(string[i],string[i+1],string[i+2],string[i+3],string[i+4],
            nAnamnesisNotes, anamnesis));
            i += 6 +nAnamnesisNotes;
        }


    }
}


