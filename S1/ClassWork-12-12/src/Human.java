

public class Human {


    private Hand hand = new Hand();
    private Nose nose = new Nose();


    private class Hand {
        public void move() {
            System.out.println("hand is moving");
        }

        public void Scratch() {
            System.out.println("scratching the  " + nose);
        }
    }

    private class Nose {
        public String toString() {
            return "i am nose";

        }
    }

    public void act(){
        moveHand();
        hand.Scratch();
    }


    public void moveHand() {
        hand.move();
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public static void main(String[] args) {
        Human h = new Human();
        h.act();
        Human.TestClass ht = new Human.TestClass();
    }


    public static class TestClass{

    }

}
