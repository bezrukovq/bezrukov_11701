/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 11
 */
public class Player {
    String name;
    String letters = "zqxjkvbpygfwmucldrhsnioate";
    int letterIndex = letters.length() - 1;
    public Player(String name){
        this.name = name;
    }

    public void makeTurn(String word, StringBuffer word_mask) {

    }

    public String getName(){ return name;}
}
