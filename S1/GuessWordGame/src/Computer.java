/**
 * @author Vladimir Bezrukov
 * 11-701
 * for Problem Set 3 Task 11
 */

public class Computer extends Player {
    public Computer(String name){
        super(name);
    }
    public void makeTurn(String word, StringBuffer word_mask){
        boolean right;
        do {
            right = false;
            char c = letters.charAt(letterIndex);
            letterIndex--;
            if (word.contains(c + "")) {
                right = true;
                System.out.println("Correct");
                for (int i = 0; i < word.length(); i++) {
                    if (word.charAt(i) == c) {
                        word_mask.setCharAt(i, c);
                    }
                }
                System.out.println("WORD: " + word_mask);
            }
        } while (right && !word.equals(word_mask.toString()));
    }
}
