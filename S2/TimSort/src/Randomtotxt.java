import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import static java.lang.Integer.min;

public class Randomtotxt {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter("Arrays.txt");
        int n;
        int a = 50;
        int inc = 150;
        Random ran = new Random();
        for (int j = 0; j < 52; j++) {
            n = a;
            writer.print(n + " ");
            for (int i = 0; i < n; i++) {
                int result = ran.nextInt(99);
                writer.print(result + " ");
            }
            if (j> 10000){
                a = 30;
                inc = 20;
            }
            a +=inc;
            inc +=100;
            writer.println();
        }

    }
}

