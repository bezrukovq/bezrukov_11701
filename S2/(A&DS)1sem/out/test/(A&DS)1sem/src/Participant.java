public class Participant implements Comparable<Participant> {
    private String name;
    private char gender;
    private Participant next;

    public Participant(String name, char gender) {
        this.name = name;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public char getGender() {
        return gender;
    }

    public Participant getNext() {
        return next;
    }

    public void setNext(Participant next) {
        this.next = next;
    }

    @Override
    public int compareTo(Participant o) {
        return this.getName().compareTo(o.getName());
    }
}
