import org.junit.Test;
import org.junit.Assert;



public class CircleListTest {
    @Test
    public void PlayerInserted(){
        CircleList cl = new CircleList("participants.txt");
        int size = cl.getSize();
        Participant p = new Participant("Azaz", 'M');
        cl.insert(p);
        Assert.assertTrue(size-cl.getSize()==-1);
        Assert.assertTrue(cl.getLast()==p);
    }

    @Test
    public void PlayerDeleted(){
        CircleList cl = new CircleList("participants.txt");
        int size = cl.getSize();
        cl.delete("Marcus");
        Assert.assertTrue(size-cl.getSize()==1);
        Assert.assertFalse(cl.getHead().getName().equals("Marcus"));
    }

    @Test
    public void PlayerSorted(){
        CircleList cl = new CircleList("participants.txt");
        cl.sort("Paul");
        Assert.assertTrue(cl.getHead().getName().equals("Paul"));
        Assert.assertTrue(cl.getLast().getName().compareTo("Paul")<0);
    }

    @Test
    public void LastIsWorking(){
        CircleList cl = new CircleList("participants.txt");
        Assert.assertTrue(cl.last(3).getName().equals("Marcus"));
        Assert.assertTrue(cl.getSize()==1);
    }

    @Test
    public void GenderIsWorking(){
        CircleList cl = new CircleList("participants.txt");
        CircleList[] cl2 = cl.gender();
        Participant p = cl2[0].getHead();
        boolean test = true;
        while (p!=cl2[0].getLast()){
            test &= p.getNext().getGender()=='M';
            p = p.getNext();
        }
        p = cl2[1].getHead();
        while (p!=cl2[1].getLast()){
            test &= p.getNext().getGender()=='W';
            p = p.getNext();
        }
        Assert.assertTrue(test);
    }

}
