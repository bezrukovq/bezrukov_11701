import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CircleList {

    private Participant last;
    private Participant head;
    private int size;


    public CircleList() {
        size = 0;
    }

    public Participant getLast() {
        return last;
    }

    public Participant getHead() {
        return head;
    }

    public CircleList(String filename) {
        try {
            Scanner sc = new Scanner(new File(filename));
            String data = sc.nextLine();
            String[] pdata = data.split(" ");
            head = new Participant(pdata[0], pdata[1].charAt(0));
            last = head;
            size++;
            while (sc.hasNext()) {
                data = sc.nextLine();
                pdata = data.split(" ");
                insert(new Participant(pdata[0], pdata[1].charAt(0)));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
            e.printStackTrace();
        }
    }

    public int getSize() {
        return size;
    }

    public void show() {
        System.out.println("_______________________________________");
        Participant participant = head;
        while (participant.getNext() != head) {
            System.out.println(participant.getName() + "   " + participant.getGender());
            participant = participant.getNext();
        }
        System.out.println(participant.getName() + "   " + participant.getGender());
        System.out.println("_______________________________________");
    }

    public void delete(String s) {
        Participant p = last;
        boolean isDeleted = false;
        for (int i = 0; i <= size && !isDeleted; i++) {
            if (p.getNext().getName().equals(s)) {
                if (p.getNext() == head)
                    head = p.getNext().getNext();
                if (p.getNext() == last)
                    last = p;
                p.setNext(p.getNext().getNext());
                size--;
                isDeleted = true;
            }
            p = p.getNext();
        }
    }


    public void insert(Participant p) {
        if (size == 0) {
            head = p;
            last = p;
        }
        last.setNext(p);
        p.setNext(head);
        last = p;
        size++;

    }

    public Participant last(int k) {
        Participant p = last;
        while (size != 1) {
            for (int i = 0; i < k; i++) {
                p = p.getNext();
            }
            delete(p.getName());
        }
        return head;
    }

    public CircleList[] gender() {
        CircleList[] cl = new CircleList[2];
        cl[0] = new CircleList();
        cl[1] = new CircleList();
        Participant participant = last;
        do {
            if (participant.getGender() == 'M') {
                cl[0].insert(new Participant(participant.getName(), participant.getGender()));
            } else
                cl[1].insert(new Participant(participant.getName(), participant.getGender()));
            participant = participant.getNext();
        } while (participant.getNext() != head);
        return cl;
    }

    public void sort(String name) {

        Participant picked = head;
        Participant least;
        Participant pre;
        Participant preleast;
        Participant prehead = last;
        boolean sortedbyname = false;
        int k = size;
        for (int i = 0; i < k; i++) {
            preleast = last;
            least = picked;
            pre = picked;
            picked = picked.getNext();
            for (int j = 0; j < size - i - 1; j++) {
                if (least.getName().compareTo(picked.getName()) > 0) {
                    least = picked;
                    preleast = pre;

                }
                pre = picked;
                picked = picked.getNext();
            }
            if (least.getName().equals(name)) {
                prehead = last;
                sortedbyname = true;
            }
            if (least == head)
                head = least.getNext();
            size--;
            preleast.setNext(least.getNext());
            this.insert(least);
            picked = head;

        }
        if (sortedbyname) {
            last = prehead;
            head = prehead.getNext();
        }
    }


    public static void main(String[] args) {
        CircleList cl = new CircleList("participants.txt");
        cl.show();
        Participant p = new Participant("Patric", 'M');
        cl.insert(p);
        cl.show();
        cl.delete("Paul");
        cl.show();
        cl.sort("Marcus");
        cl.show();
        CircleList[] cl2 = cl.gender();
        cl2[1].show();
        cl2[0].show();
        cl.last(5);
        cl.show();

    }
}
