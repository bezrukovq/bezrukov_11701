package singleton;

import java.io.IOException;
import java.util.List;

public class Task01 {
    public static String getWordWithMaxUsageLoopStyle() throws IOException {
        List<WordData> wdc = WordDataCollectionSingleton.getWordDataCollection();
        if(wdc.size() ==0){
            return null;
        }
        int max = wdc.get(0).getCount();
        String word = wdc.get(0).getWord();
        for(WordData x: wdc){
            if(x.getCount()> max) {
                max = x.getCount();
                word = x.getWord();
            }
        }
        return word;
    }

    public static void main(String[] args) throws IOException {
        System.out.println(getWord());
        System.out.println(getWordWithMaxUsageLoopStyle());

    }

    public static String getWord() throws IOException {
        String word = WordDataCollectionSingleton.getWordDataCollection()
                .stream()
                .reduce((wd1,wd2) -> {return wd1.getCount()>wd2.getCount() ? wd1:wd2;})
                .orElse(new WordData("",0))
                .getWord();
        return word;
    }
}
