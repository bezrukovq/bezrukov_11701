package singleton;

public class WordData {
    private String word;
    private int count;

    public WordData(String word, int i) {
        this.word = word;
        this.count = i;
    }

    public String getWord() {

        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
