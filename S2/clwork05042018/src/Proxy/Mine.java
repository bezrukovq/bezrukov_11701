package Proxy;

public class Mine extends Cell {

    public Mine(int left, int top) {
        super(left, top);

    }

    @Override
    public int getPoints() {
        return 100;
    }
}
