package Adapter;

public class main {
    public static void main(String[] args) {
        RoundHole hole = new RoundHole(5);
        Round rpeg = new Round(5);
        if (hole.fits(rpeg)) {
            System.out.println("Round peg r5 fits round hole r5.");
        }

        Square smallSq = new Square(2);
        Square largeSq = new Square(20);
        SquareToRoundAdapter smallSqAdapter = new SquareToRoundAdapter(smallSq);
        SquareToRoundAdapter largeSqAdapter = new SquareToRoundAdapter(largeSq);
        if (hole.fits(smallSqAdapter)) {
            System.out.println("Square peg w2 fits round hole r5.");
        }
        if (!hole.fits(largeSqAdapter)) {
            System.out.println("Square peg w20 does not fit into round hole r5.");
        }
    }
}
