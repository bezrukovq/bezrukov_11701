package Adapter;


    public class SquareToRoundAdapter extends Round {
        private Square peg;

        public SquareToRoundAdapter(Square peg) {
            this.peg = peg;
        }

        @Override
        public double getRadius() {
            double result;
            result = (Math.sqrt(Math.pow((peg.getWidth() / 2), 2) * 2));
            return result;
        }
    }

