package Decorator;

public class Main {
    public static void main(String[] args) {
        Order fourSeasonsPizza = new Pizza("Four Seasons Pizza", 10);
        fourSeasonsPizza = new Extra("Pepperoni", 4, fourSeasonsPizza);
        fourSeasonsPizza = new Extra("Mozzarella", 2, fourSeasonsPizza);
        fourSeasonsPizza = new Extra("Chili", 2, fourSeasonsPizza);

        System.out.println(fourSeasonsPizza.getPrice());
        System.out.println(fourSeasonsPizza.getLabel());
    }
}
