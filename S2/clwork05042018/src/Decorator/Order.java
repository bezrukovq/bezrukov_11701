package Decorator;

public interface Order {

    public double getPrice();
    public String getLabel();

}
