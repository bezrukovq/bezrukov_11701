package FactoryMethod;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner sc = Factory.getScanner();
        System.out.println(sc.nextLine());
    }
}
