public class Elem {
    private Object value;
    private Elem next;

    public Elem(Object value) {
        this.value = value;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    public Object getValue() {
        return value;
    }

    public Elem getNext() {
        return next;
    }
}
