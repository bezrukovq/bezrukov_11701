import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        Elem head = null;
        int num = 5;
        for(int i = 0; i<num; i++) {
            Elem p = new Elem(sc.nextLine());
            p.setNext(head);
            head = p;
        }
        Elem obj = head;
        int i = sc.nextInt();
        for(int j = 0; j<num - i; j++){
            obj = obj.getNext();
        }
        System.out.println(obj.getValue());
    }
}
