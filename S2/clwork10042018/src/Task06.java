import entities.CarData;
import singletons.CarDataCollectionSingleton;

import java.util.List;

public class Task06 {
    public static void main(String[] args) {
        System.out.println(averagebyModel("volkswagen"));
        System.out.println(averagebyMaker("volkswagen"));
    }

    public static double averagebyModel(String model) {
        List<CarData> cardata = CarDataCollectionSingleton.getInstance();
        return cardata.stream()
                .filter(a -> a.getCar().getModel().getName().equals(model))
                .mapToInt(CarData::getHorsepower).average().getAsDouble();
    }

    public static double averagebyMaker(String maker) {
        List<CarData> cardata = CarDataCollectionSingleton.getInstance();
        return cardata.stream()
                .filter(a -> a.getCar().getModel().getMaker().getName().equals(maker))
                .mapToInt(CarData::getHorsepower).average().getAsDouble();
    }
}
