import entities.CarData;
import entities.CarMaker;
import singletons.CarDataCollectionSingleton;
import singletons.CarMakerCollectionsSingleton;

import java.util.List;
import java.util.stream.Collectors;

public class Task05 {
    public static void main(String[] args) {
        getMakersbefore(1971);
    }


    public static void getMakersbefore(int year) {
        List<CarData> data = CarDataCollectionSingleton.getInstance();
        data.stream()
                .filter(a -> a.getYear()<year)
                .map(a-> a.getCar().getModel().getMaker())
                .distinct()
                .forEach(a-> System.out.println(a));
    }
}
