import entities.Model;
import singletons.CarMakerCollectionsSingleton;
import singletons.CountryCollectionSingleton;
import singletons.ModelsCollectionsSingleton;

import java.util.List;
import java.util.stream.Collectors;

public class Task04 {
    public static void main(String[] args) {
        System.out.println(countModels("germany"));
        System.out.println(CountryCollectionSingleton.getInstance().stream()
        .collect(
                Collectors.toMap(
                        x->x,
                        x -> countModels(x.getName())
                )
        ));
    }
    public static long countModels(String s){
        List<Model> models = ModelsCollectionsSingleton.getInstance();
        return models.stream().filter(a -> a.getMaker().getCountry().getName().equals(s)).count();

    }
}
