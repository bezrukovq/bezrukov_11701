package singletons;

import entities.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class Reader {
    private static String trim(String str) {
        return (String) Stream.of(str.split("'"))
                .filter((str1) -> !str1.equals(""))
                .toArray()[0]
        ;
    }


    public static List<Continent> readContinents(final String FILE_NAME) {
        List<Continent> continents;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            continents = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {

                    String[] strings = line.split(",");
                    continents.add(
                            new Continent(
                                    Integer.parseInt(strings[0]),
                                    trim(strings[1])
                            )
                    );
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            continents = null;
            e.printStackTrace();
        }
        return continents;
    }


    public static List<Country> readCountries(final String FILE_NAME) {
        List<Country> countries;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            countries = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {

                    String[] strings = line.split(",");

                    int id = Integer.parseInt(strings[0]);
                    String name = trim(strings[1]);

                    Continent continent = null;
                    if (!strings[2].equals("null")) {
                        int continentId = Integer.parseInt(strings[2]);
                        // find continent by id
                        continent = ContinentCollectionSingleton.getInstance().stream().filter((continent1 -> continent1.getId() == continentId)).findAny().get();
                    }

                    Country country = new Country(id, name, continent);
                    countries.add(country);
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            countries = null;
            e.printStackTrace();
        }
        return countries;
    }


    public static List<CarMaker> readCarMakers(final String FILE_NAME) {
        List<CarMaker> carMakers;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            carMakers = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");

                    int id = Integer.parseInt(strings[0]);

                    Country country = null;
                    if (!strings[3].equals("null")) {
                        int countryId = Integer.parseInt(strings[3]);
                        // find country by id
                        country = CountryCollectionSingleton.getInstance().stream().filter((country1 -> country1.getId() == countryId)).findAny().get();
                    }
                    CarMaker carMaker = new CarMaker(id, trim(strings[1]), trim(strings[2]), country);
                    carMakers.add(carMaker);
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            carMakers = null;
            e.printStackTrace();
        }
        return carMakers;
    }


    public static List<CarName> readCarNames(final String FILE_NAME) {
        List<CarName> carNames;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            carNames = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");
                    carNames.add(
                            new CarName(
                                    Integer.parseInt(strings[0]),
                                    ModelsCollectionsSingleton.getInstance().stream()
                                            .filter(a -> a.getName().equals(trim(strings[1])) )
                                            .findAny().get(),
                                    trim(strings[2])
                            )
                    );
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(line);
                    line = bufferedReader.readLine();
                    continue;
                }
            }
        } catch (IOException e) {
            carNames = null;
            e.printStackTrace();
        }
        return carNames;
    }

    public static List<Model> readModels(String FILE_NAME) {
        List<Model> models;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            models = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");
                    int i =Integer.parseInt(strings[1]);
                    CarMaker cmaker = CarMakerCollectionsSingleton.getInstance().stream().filter((c1 -> c1.getId() == i)).findFirst().get();

                    models.add(
                            new Model(
                                    Integer.parseInt(strings[0]),
                                    cmaker,
                                    trim(strings[2])
                            )
                    );
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            }
        } catch (IOException e) {
            models = null;
            e.printStackTrace();
        }
        return models;
    }

    public static List<CarData> readCarData(String FILE_NAME) {
        List<CarData> carData;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            carData = new LinkedList<>();
            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");
                    int[] data= new int[7];
                    double data5;
                    try {
                        data[0] = Integer.parseInt(strings[1]);
                    } catch (NumberFormatException e){
                        data[0]=0;
                    }
                    try {
                        data[1] = Integer.parseInt(strings[2]);
                    } catch (NumberFormatException e){
                        data[1]=0;
                    }
                    try {
                        data[2] = Integer.parseInt(strings[3]);
                    } catch (NumberFormatException e){
                        data[2]=0;
                    }
                    try {
                        data[3] = Integer.parseInt(strings[4]);
                    } catch (NumberFormatException e){
                        data[3]=0;
                    }
                    try {
                        data[4] = Integer.parseInt(strings[5]);
                    } catch (NumberFormatException e){
                        data[4]=0;
                    }
                    try {
                        data[6] = Integer.parseInt(strings[7]);
                    } catch (NumberFormatException e){
                        data[6]=0;
                    }
                    try{
                        data5 = Double.parseDouble(strings[6]);
                    } catch (NumberFormatException e){
                        data5 =0;
                    }
                    carData.add(
                            new CarData(
                                    CarNameCollectionSingleton.getInstance().stream()
                                            .filter(a -> a.getId()== Integer.parseInt(strings[0]))
                                            .findAny().get(),
                                    data[0],
                                    data[1],
                                    data[2],
                                    data[3],
                                    data[4],
                                    data5,
                                    data[6])
                    );
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    e.printStackTrace();
                    line = bufferedReader.readLine();
                    continue;
                }
            }
        } catch (IOException e) {
            carData = null;
            e.printStackTrace();
        }
        return carData;
    }
}
