package singletons;

import entities.CarData;

import java.util.List;

public class CarDataCollectionSingleton {
    private static final String FILE_NAME = "res/CARS/cars-data.csv";
    private static List<CarData> instance;

    public static List<CarData> getInstance() {
        if (instance == null) {
            instance = Reader.readCarData(FILE_NAME);
        }
        return instance;
    }
}
