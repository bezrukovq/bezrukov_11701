package singletons;

import entities.CarMaker;
import entities.Model;

import java.util.List;

public class ModelsCollectionsSingleton {
    private static final String FILE_NAME = "res/CARS/model-list.csv";
    private static List<Model> instance;

    public static List<Model> getInstance() {
        if (instance == null) {
            instance = Reader.readModels(FILE_NAME);
        }
        return instance;
    }
}
