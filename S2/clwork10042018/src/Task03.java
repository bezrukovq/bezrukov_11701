import entities.CarMaker;
import singletons.CarMakerCollectionsSingleton;
import singletons.ContinentCollectionSingleton;
import singletons.CountryCollectionSingleton;

import java.util.List;
import java.util.stream.Collectors;

public class Task03 {
    public static void main(String[] args) {
        getMakers("europe");

    }

    public static void getMakers(String s) {
        List<CarMaker> cntr = CarMakerCollectionsSingleton.getInstance();
        cntr.stream()
                .filter(a -> a.getCountry().getContinent().getName().equals(s))
                .forEach(System.out::println);
    }
}
