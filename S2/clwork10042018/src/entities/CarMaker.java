package entities;

public class CarMaker {
    private int id;
    private String name;
    private String fullName;
    private Country country;

    public CarMaker(int id, String name, String fullName, Country country) {
        this.id = id;
        this.name = name;
        this.fullName = fullName;
        this.country = country;
    }
    public String toString(){
        return this.getName();
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
