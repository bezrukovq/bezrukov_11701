package entities;

public class CarData {
    private CarName car;
    private int MPG;
    private int cylinders;
    private int edispl;
    private int horsepower;
    private int weight;
    private double accelerate;
    private int year;

    public CarData(CarName car, int MPG, int cylinders, int edispl, int horsepower, int weight, double accelerate, int year) {
        this.car = car;
        this.MPG = MPG;
        this.cylinders = cylinders;
        this.edispl = edispl;
        this.horsepower = horsepower;
        this.weight = weight;
        this.accelerate = accelerate;
        this.year = year;
    }

    public CarName getCar() {
        return car;
    }

    public void setCar(CarName car) {
        this.car = car;
    }

    public int getMPG() {
        return MPG;
    }

    public void setMPG(int MPG) {
        this.MPG = MPG;
    }

    public int getCylinders() {
        return cylinders;
    }

    public void setCylinders(int cylinders) {
        this.cylinders = cylinders;
    }

    public int getEdispl() {
        return edispl;
    }

    public void setEdispl(int edispl) {
        this.edispl = edispl;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getAccelerate() {
        return accelerate;
    }

    public void setAccelerate(double accelerate) {
        this.accelerate = accelerate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
