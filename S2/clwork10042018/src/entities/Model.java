package entities;

public class Model {
    private int id;
    private String name;
    private CarMaker maker;

    public Model(int id,CarMaker maker, String name) {
        this.id = id;
        this.name = name;
        this.maker = maker;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CarMaker getMaker() {
        return maker;
    }

    public void setMaker(CarMaker maker) {
        this.maker = maker;
    }
}
