package entities;

public class CarName {
    private int id;
    private Model model;
    private String make;

    public CarName(int id, Model model, String make) {
        this.id = id;
        this.model = model;
        this.make = make;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }
}
