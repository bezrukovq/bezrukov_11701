package tests;

import entities.WordData;
import singletons.WordDataCollectionSingleton;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class WordDataTest {
    @Test
    public void testCorrectLength() throws IOException {
        List<WordData> wdc = WordDataCollectionSingleton.getWordDataCollection();
        Assert.assertEquals(27469, wdc.size());
    }
}
