import entities.Continent;
import entities.Country;
import singletons.ContinentCollectionSingleton;
import singletons.CountryCollectionSingleton;


import java.util.Arrays;
import java.util.List;

public class Task02 {
    public static void main(String[] args) {
        System.out.println(getMaxCountryLoopStyle());
        System.out.println(getMaxCountryStreamStyle());
    }

    public static String getMaxCountryLoopStyle(){
        int n = ContinentCollectionSingleton.getInstance().size();
        int[] arr = new int[n];
        List<Country> cntr = CountryCollectionSingleton.getInstance();
        for(Country x : cntr){
            arr[x.getContinent().getId()-1]++;
        }
        int a =0;
        for(int i=1; i<arr.length; i++){
            if(arr[i]>arr[a]){
                a=i;
            }
        }

        return ContinentCollectionSingleton.getInstance().get(a).getName();
    }

    public static int getNumofCountries(String s){
        List<Country> cntr = CountryCollectionSingleton.getInstance();
        int i=0;
        for(Country x : cntr){
            if(x.getContinent().getName().equals(s))
                i++;
        }
        return i;
    };

    public static String getMaxCountryStreamStyle(){
        String cntr = ContinentCollectionSingleton.getInstance()
                .stream()
                .reduce(((x1, x2) -> {return getNumofCountries(x1.getName())>getNumofCountries(x2.getName())? x1 : x2;}))
                .orElse(new Continent(0,"")).getName();
        return  cntr;
    }
}
