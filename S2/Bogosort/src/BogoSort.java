import java.util.Arrays;

public class BogoSort {
    public static int shuffles = 172001;

    public static void main(String[] args) {
        int tries = 0;
        while (shuffles > 7) {
            int[] arr = {4, 8, 15, 16, 23, 42, 5, 9, 17, 18};
            tries++;
            System.out.print("Unsorted: ");
            System.out.println(Arrays.toString(arr));

            BogoSort.bogo(arr);

            System.out.print("Sorted: ");
            System.out.println(Arrays.toString(arr));

        }
    }

    static void bogo(int[] arr) {
        shuffles = 1;
        while (!isSorted(arr)) {
            shuffles++;
            shuffle(arr);
            if(shuffles>8){
                break;
            }
        }
        System.out.println(shuffles);
    }

    static void shuffle(int[] arr) {
        int i = arr.length - 1;
        while (i > 0)
            swap(arr, i--, (int) (Math.random() * i));
    }

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static boolean isSorted(int[] arr) {

        for (int i = 1; i < arr.length; i++)
            if (arr[i] < arr[i - 1])
                return false;
        return true;
    }

}