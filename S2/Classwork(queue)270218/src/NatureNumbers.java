import java.util.Scanner;

public class NatureNumbers {
    public static Queue<Integer>[] q = new Queue[3];

    public static void main(String[] args) {
        q[0] = new Queue<>();
        q[1] = new Queue<>();
        q[2] = new Queue<>();
        q[0].push(2);
        q[1].push(3);
        q[2].push(5);
        int picked = q[0].pop();
        int oldpick = 0;
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        while (picked <= n) {
            oldpick = picked;
            System.out.println(picked);
            q[0].push(2 * picked);
            q[1].push(3 * picked);
            q[2].push(5 * picked);
            do {
                picked = getM();
            } while (picked == oldpick);
        }
    }

    public static int getM() {
        return q[0].peek() < q[1].peek() && q[0].peek() < q[2].peek() ? q[0].pop() : q[1].peek() < q[2].peek() ? q[1].pop() : q[2].pop();
    }
}
