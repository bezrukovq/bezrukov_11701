public class Queue<T>{
    private int size;
    private Elem head;
    private Elem last;

    private class Elem{
        T value;
        Elem next;
        Elem(T i){
            value = i;
        }
    }

    public Queue(){
        size = 0;
        head = null;
        last = null;
    }

    public boolean empty(){
        return size==0;
    }

    public T peek(){
        return head.value;
    }

    public void push(T i){
        Elem e = new Elem(i);
        if(empty()){
            head = e;
            last = e;
        }
        last.next = e;
        last = e;
        size++;
    }

    public T pop(){
        if(!empty()) {
            T hvalue = head.value;
            head = head.next;
            size--;
            return hvalue;
        }
        return null;
    }
}
