public class DStackQueue<T> {
    private Stack<T> stack;
    private Stack<T> queue;

    public DStackQueue() {
        stack = new Stack<>();
        queue = new Stack<>();
    }

    public void push(T p){
        stack.push(p);
    }
    public T pop(){
        if(queue.empty()){
            int j = stack.getSize();
            for(int i = 0; i<j;i++){
                queue.push(stack.pop());
            }
        }
        return queue.pop();
    }
    public boolean empty(){
        return queue.empty() && stack.empty();
    }

}
