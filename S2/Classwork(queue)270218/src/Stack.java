
public class Stack<T> {

    private int size;
    private Elem head;

    private class Elem{
        T value;
        Elem next;
        Elem(T i){
            value = i;
        }

    }

    public Stack(){
        size = 0;
        head = null;
    }

    public boolean empty(){
        return size==0;
    }

    public T peek(){
        return head.value;
    }

    public void push(T i){
        Elem e = new Elem(i);
        e.next = head;
        head = e;
        size++;
    }

    public T pop(){
        if(!empty()) {
            T hvalue = head.value;
            head = head.next;
            size--;
            return hvalue;
        }
        return null;
    }
    public int getSize(){
        return size;
    }


}
