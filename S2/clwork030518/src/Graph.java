import java.util.ArrayList;
import java.util.List;

public class Graph {
    private List<Node> vertexs;
    private List<Edge> edges;


    public Graph(List<Object> objects) {
        if (objects instanceof Node) {
            for (Object x : vertexs)
                this.vertexs.add((Node) x);
        } else if (objects instanceof Edge)
            this.vertexs = convertEdgesToGraph(edges);
    }

    public List<Node> convertEdgesToGraph(List<Edge> edges) {
        List<Node> vertexs = new ArrayList<>();
        for (Edge edge : edges) {
            if (!hasVertex(edge.getNode1().getValue())) {
                vertexs.add(edge.getNode1());
                edge.getNode1().getNeighbors().add(edge.getNode2());
            }
            if (!hasVertex(edge.getNode2().getValue())) {
                vertexs.add(edge.getNode2());
                edge.getNode2().getNeighbors().add(edge.getNode1());
            }
        }
        return vertexs;
    }


    public void addVertex(int value) {
        if (!hasVertex(value)) {
            vertexs.add(new Node(value));
        }
    }

    public boolean hasVertex(int value) {
        for (Node node : vertexs) {
            if (node.getValue() == value)
                return true;
        }
        return false;
    }

    public Node getNodeByValue(int value) {
        for (Node node : vertexs) {
            if (node.getValue() == value)
                return node;
        }
        return null;
    }

    public boolean hasEdge(int value1, int value2) {
        if (!hasVertex(value1))
            return false;
        for (Node node : getNodeByValue(value1).getNeighbors()) {
            if (node.getValue() == value2)
                return true;
        }
        return false;
    }

    public void addEdge(int value1, int value2) {
        if (!hasVertex(value1))
            addVertex(value1);
        if (!hasVertex(value2))
            addVertex(value2);
        getNodeByValue(value1).getNeighbors().add(new Node(value1));
        getNodeByValue(value2).getNeighbors().add(new Node(value2));
        edges.add(new Edge(getNodeByValue(value1), getNodeByValue(value2)));
    }


    public List<Edge> convertGraphToEdges() {
        edges = new ArrayList<>();
        for (Node vertex : vertexs) {
            for (Node x : vertex.getNeighbors()) {
                addEdge(vertex.getValue(), x.getValue());
            }
        }
        return edges;
    }
}
