import java.util.ArrayList;
import java.util.List;

public class Node {
    private List<Node> neighbors = new ArrayList<Node>();

    private int value;

    public Node(int value) {
        this.value = value;
    }

    public List<Node> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(List<Node> neighbors) {
        this.neighbors = neighbors;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
