import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.Semaphore;

public class FileDownloader extends Thread {
    private static Semaphore semaphore = new Semaphore(3,true);
    private String fileURL = "";
    private List<String> links;
    public static byte counter = 0;

    public FileDownloader(List<String> links) {
        this.links = links;
    }

    public FileDownloader(String fileURL) {
        this.fileURL = fileURL;
    }
    public void run(){
        try {
            semaphore.acquire();
            System.out.println("download started");
            download();
            semaphore.release();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void download() throws IOException {
        counter++;
        if (fileURL != "") {
            downloadByString(fileURL);
        } else {
            for (String n : links)
                downloadByString(n);
        }
    }

    private void downloadByString(String Urrl) throws IOException {
        URL url = new URL(Urrl);
        String urlll = url.getFile();
        String arr[] = urlll.split("/");
        String name = arr[arr.length-1];
        BufferedInputStream bis = new BufferedInputStream(url.openStream());
        FileOutputStream fis = new FileOutputStream("C:\\BB\\ии\\somePdf" +
                name);
        byte[] buffer = new byte[32];
        int count;
        while ((count = bis.read(buffer, 0, 32)) != -1) {
            fis.write(buffer, 0, count);
        }

        System.out.println("download fnished");
        fis.close();
        bis.close();
    }
}
