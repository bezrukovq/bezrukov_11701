import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class    Downloader {

    private URL url;
    private String ext;
    private final static String RE__TEMPLATE_FOR_URL="";
    private final String reForURL;

    public String buildReForURL(){
        //TODO building re with template and extention
        return "";
    }

    public Downloader(URL url, String ext) {
        this.url = url;
        this.ext = ext;
        this.reForURL= buildReForURL();
    }

    public void start() throws IOException {
        List<String> links =getLinks(new LineNumberReader(new InputStreamReader(url.openStream())));
        //new FileDownloader(links).download();
    }
    public List<String> getLinks(LineNumberReader lnr) throws IOException {
        String line = lnr.readLine();
        List<String> links = new LinkedList<>();
        byte counter = 0;
        while (line != null) {
            Pattern p = Pattern.compile("\"(https?|ftp)://[^\\s/$.?#].[^\\s]*."+ext+"\"");
            Matcher m = p.matcher(line);
            while (m.find()){
                counter++;

                String fileURL = m.group();
                fileURL = fileURL.replace("\"","");
                links.add(fileURL);
                System.out.println(fileURL);
                if(counter != 3 && counter < 5)
                (new FileDownloader(fileURL)).start();

            }
            line = lnr.readLine();
        }
        return links;
    }
}
