
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
public class TaskUrl {
    public static void main(String[] args) throws IOException {
        URL url= null;
        try {
            url = new URL("https://en.wikipedia.org/wiki/London");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String ext = "pdf";

        (new Downloader(url,ext)).start();

    }



}
