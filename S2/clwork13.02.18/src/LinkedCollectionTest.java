import collections.*;
import org.junit.Assert;
import org.junit.Test;


public class LinkedCollectionTest {
    @Test
    public void newCollectionShouldHaveSize0() {
        IntLinkedCollection coll = new IntLinkedCollection();
        Assert.assertEquals(0, coll.size());
    }

    @Test
    public void newCollectionIsEmpty(){
        IntLinkedCollection coll = new IntLinkedCollection();
        Assert.assertTrue(coll.isEmpty());
    }

    @Test
    public void ElementRemoved(){
        IntLinkedCollection coll = new IntLinkedCollection();
        coll.add(6);
        coll.add(7);
        coll.add(5);
        coll.remove(5);
        Assert.assertTrue(!coll.contains(5));
    }

    @Test
    public void newELementAdded(){
        IntLinkedCollection coll = new IntLinkedCollection();
        coll.add(5);
        Assert.assertTrue(coll.size()==1);
        Assert.assertTrue(!coll.isEmpty());
        Assert.assertTrue(coll.contains(5));
    }


}
