package collections;

import org.omg.CORBA.INTERNAL;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class IntArrayCollection extends AbstractCollection implements Collection<Integer> {
    private final int CAPACITY = 200;
    private Integer[] arr = new Integer[CAPACITY];
    private int size = 0;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {

        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof Integer) {
            int b = (Integer) o;
            for (int i = 0; i < size; i++) {
                if (arr[i] == b) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    @Override
    public void clear() {
        size = 0;
    }

    @Override
    public boolean add(Integer integer) {
        if (this.contains(integer))
            return false;
        if (size == CAPACITY) {
            reallocate();
        }
        arr[size++] = integer;
        return true;
    }
    //reallocate sdelat

    private void reallocate() {

        Integer[] arrr = Arrays.copyOf(arr, 2 * arr.length);
        arr = arrr;
    }

    @Override
    public boolean remove(Object o) {
        int b = (Integer) o;
        for (int i = 0; i < size; i++) {
            if (arr[i] == b) {
                for (int j = i; j < size; j++)
                    arr[j] = arr[j + 1];
                size--;
            }

        }
        return false;
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return arr;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }



    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

}
