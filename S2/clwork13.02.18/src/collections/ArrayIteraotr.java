package collections;

import java.util.Iterator;

public class ArrayIteraotr implements Iterator<Integer> {
    private Integer [] arr;
    private int cursor =0;
    public ArrayIteraotr(Integer [] arr){
        this.arr = arr;
        cursor = 0;
    }


    @Override
    public boolean hasNext() {

        return cursor<arr.length;
    }

    @Override
    public Integer next() {
        return arr[cursor++];
    }
}
