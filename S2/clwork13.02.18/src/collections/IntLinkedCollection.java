package collections;

import java.util.Collection;
import java.util.Iterator;

public class IntLinkedCollection extends AbstractCollection implements Collection<Integer>  {

    private class Elem {
        Integer value;
        Elem next;
    }

    private Elem head;
    private int n = 0;

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof Integer) {
            Integer i = (Integer) o;
            Elem p = head;
            while (p != null) {
                if (p.value == i) {
                    return true;
                }
                p = p.next;
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer i) {
        Elem p = new Elem();
        p.value = i;
        p.next = head;
        head = p;
        n++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (o instanceof Integer) {
            Integer i = (Integer) o;
            Elem p = head;
            if(n ==1){
                if (p.value == i){
                    p.value = null;
                    n--;
                }
            }
            if(i == head.value){
                head= head.next;
                n--;
            }
            do {
                if (p.value == i) {
                    p.value = p.next.value;
                    p.next = p.next.next;
                    n--;
                    return true;
                }
                p = p.next;
            } while (p != null);
            return false;
        } else {
            return false;
        }
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        head = null;
    }
}
