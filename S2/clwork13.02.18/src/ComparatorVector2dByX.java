import java.util.Comparator;

public class ComparatorVector2dByX implements Comparator<Vector2D> {
    @Override
    public int compare(Vector2D o1, Vector2D o2) {
        if(o1.getX() - o2.getX()>0){
            return 1;
        } else if(o1.getX() - o2.getX()<0){
            return -1;
        }

        return  0;
    }
}
