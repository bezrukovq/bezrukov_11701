import collections.*;
import org.junit.Assert;
import org.junit.Test;

public class ArrayCollectiontest {
    @Test
    public void newCollectionShouldHaveSize0() {
        IntArrayCollection coll = new IntArrayCollection();
        Assert.assertEquals(0, coll.size());
    }

    @Test
    public void newCollectionIsEmpty(){
        IntArrayCollection coll = new IntArrayCollection();
        Assert.assertTrue(coll.isEmpty());
    }

    @Test
    public void ElementRemoved(){
        IntArrayCollection coll = new IntArrayCollection();
        coll.add(5);
        coll.remove(5);
        Assert.assertTrue(!coll.contains(5));
    }

    @Test
    public void newELementAdded(){
        IntArrayCollection coll = new IntArrayCollection();
        coll.add(5);
        Assert.assertTrue(coll.size()==1);
        Assert.assertTrue(!coll.isEmpty());
        Assert.assertTrue(coll.contains(5));
    }


}
