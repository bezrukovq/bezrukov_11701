public class Task4 {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        int first;
        int second;
        String postFixString = "2 3 5 + * 6 + 2 /";
        for (int i = 0; i < postFixString.length(); i++){
            if(Character.isDigit(postFixString.charAt(i))){
                
                stack.push(Character.getNumericValue(postFixString.charAt(i)));
            } else
            switch (postFixString.charAt(i)){
                case '+':
                    stack.push(stack.pop()+stack.pop());
                    break;
                case '-':
                    second = stack.pop();
                    first = stack.pop();
                    stack.push(first-second);
                    break;
                case '*':
                    stack.push(stack.pop()*stack.pop());
                    break;
                case '/':
                    second = stack.pop();
                    first = stack.pop();
                    stack.push(first/second);
                    break;
            }
        }
        System.out.println(stack.pop());
    }
}
