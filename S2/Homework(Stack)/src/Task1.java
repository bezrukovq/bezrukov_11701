import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        int[] arr ={1,2,3,4,5,6,7,8,9};
        for (int i = 0; i<arr.length; i++){
            stack.push(arr[i]);
        }
        for (int i = 0; i<arr.length; i++){
            arr[i] = stack.pop();
        }
        System.out.println(Arrays.toString(arr));

    }
}
