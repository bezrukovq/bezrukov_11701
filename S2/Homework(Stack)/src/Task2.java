public class Task2 {
    public static void main(String[] args) {
        boolean check;
        int closed;
        int opened;
        String ch;
        for (String n : args) {
            closed = 0;
            opened = 0;
            for (int i = 0; i < n.length(); i++) {
                ch = String.valueOf(n.charAt(i));
                if (ch.equals("(")) {
                    opened++;
                } else if (ch.equals(")")) {
                    closed++;
                }
                if (closed > opened) {
                    closed = -1;
                    break;
                }
            }
            check = closed == opened;
            System.out.println(check);
        }
    }

}
