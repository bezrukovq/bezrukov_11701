import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.Semaphore;


public class Task02 {
	private static Semaphore semaphore = new Semaphore(3,true);

    public static void main(String[] args) {
        String str1 = "notes";
        String str2 = "news";
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(str1);
        arrayList.add(str2);
        copyAll(arrayList);

    }

    public static void copyAll(ArrayList<String> arrayList) {
        for (String x : arrayList) {
            Thread1 tr1 = new Thread1(x);
            tr1.start();
        }
    }

    public static void copyFile(String filename) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("rsc/" + filename + ".txt"));
        try (FileWriter writer = new FileWriter("rsc/" + filename + "Copy.txt")) {
            semaphore.acquire();
            while (sc.hasNext()) {
                String text = sc.nextLine();
                writer.write(text + "\n");
            }
            writer.close();
            semaphore.release();


        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }
}
