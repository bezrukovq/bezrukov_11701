import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("rsc/notes.txt"));
        try(FileWriter writer = new FileWriter("rsc/notesCopy.txt"))
        {
            while (sc.hasNext()) {
                String text = sc.nextLine();
                writer.write(text+"\n");
            }
            writer.close();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
}
