import java.io.FileNotFoundException;

public class Thread1 extends Thread {
    String filename;
    public Thread1(String filename){
        this.filename = filename;
    }
    public void run(){
        try {
            Task02.copyFile(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
