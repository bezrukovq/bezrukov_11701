import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Task02 {
    public static void main(String[] args) throws FileNotFoundException {
        Map<String, Integer> map = new HashMap<>();
        Scanner sc = new Scanner(new File("engtext.txt"));
        while (sc.hasNext()){
            String str = sc.next();
            str = str.toLowerCase();
            map.put(str, map.get(str) == null ? 1 : map.get(str) +1);

        }
        List list = new ArrayList(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return b.getValue()-a.getValue();
            }
        });

        System.out.println(list);
    }
}
