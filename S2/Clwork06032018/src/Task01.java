import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) throws FileNotFoundException {
        Map<Character, Integer> map = new HashMap<>();
        Scanner sc = new Scanner(new File("engtext.txt"));
        while (sc.hasNext()){
            String str = sc.nextLine();
            for(int i = 0; i<str.length(); i++){
                if(Character.isLowerCase(str.charAt(i))){
                    map.put(str.charAt(i), map.get(str.charAt(i)) == null ? 1 : map.get(str.charAt(i)) +1);
                }
            }
        }
        System.out.println(map);
    }
}
