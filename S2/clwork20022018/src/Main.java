import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> integ = new ArrayList<>();
        integ.add(251);
        integ.add(729);
        integ.add(236);
        Collections.sort(integ, new IntegerComparator());
        System.out.println(integ);
        List<Integer[]> arrays = new ArrayList<>();
        arrays.add(new Integer[]{1,2,3,4});
        arrays.add(new Integer[]{1,2,3,4});
        Collections.sort(arrays, (o1,o2)->{
            for(int i = 0; i<o1.length; i++){
                if(o1[i]>o2[i]){
                    return 1;
                } else if(o1[i]<o2[i])
                    return -1;
            }
            return 0;
        });
    }
}
