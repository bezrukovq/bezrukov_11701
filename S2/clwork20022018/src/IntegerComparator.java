import java.util.Comparator;

public class IntegerComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer o1, Integer o2) {
        int oo1 = o1;
        int oo2 = o2;
        if (length(oo1) == length(oo2)) {
            while (oo1 != 0) {
                if (oo1 % 10 > oo2 % 10) {
                    return 1;
                } else if (oo1 % 10 < oo2 % 10){
                    return -1;
                }
                oo1/=10;
                oo2/=10;
            }
            return 0;
        } else if (length(oo1) > length(oo2)){
            return 1;
        } else if (length(oo1) < length(oo2))
            return -1;
        return 0;
    }

    private int length(int i) {
        int num = 0;
        do {
            i /= 10;
            num++;
        } while (i != 0);
        return num;
    }

}
