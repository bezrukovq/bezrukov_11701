import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Vector2D implements Comparable<Vector2D> {
    private double x;
    private double y;

    public Vector2D() {
        x = 0;
        y = 0;
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D add(Vector2D Vector) {
        Vector2D sumVector = new Vector2D();
        sumVector.setX(this.x + Vector.getX());
        sumVector.setY(this.y + Vector.getY());
        return sumVector;
    }

    public Vector2D sub(Vector2D Vector) {
        Vector2D subVector = new Vector2D();
        subVector.setX(this.x - Vector.getX());
        subVector.setY(this.y - Vector.getY());
        return subVector;
    }

    public Vector2D mult(double коэффицент_уможения) {
        Vector2D multVector = new Vector2D();
        multVector.setX(this.x * коэффицент_уможения);
        multVector.setY(this.y * коэффицент_уможения);
        return multVector;
    }

    public double scalarProduct(Vector2D v2) {
        return v2.getY() * this.y + v2.getX() * this.x;
    }

    public String toString() {
        return "x : " + x + " \n y :" + y+"\n";
    }

    public double length() {
        return Math.sqrt(x * x + y * y);
    }


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }


    @Override
    public int compareTo(Vector2D o) {
        if(this.getX() - o.getX()>0){
            return 1;
        } else if(this.getX() - o.getX()<0){
            return -1;
        } else if(this.getY() - o.getY()>0){
            return 1;
        } else if(this.getY() - o.getY()<0){
            return -1;
        }
        return  0;
    }

    public static void main(String[] args) {
        Vector2D vec = new Vector2D(5,4);
        Vector2D vec2 = new Vector2D(5,5);
        System.out.println(vec.compareTo(vec2));
        List<Vector2D> vectors = new ArrayList<>();
        vectors.add(vec);
        vectors.add(vec2);
        vectors.add(new Vector2D(5,10));
        Collections.sort(vectors);
        System.out.println(vectors);
        //without anonumys class
        //Collections.sort(vectors, new ComparatorVector2dByX());
        //System.out.println(vectors);

        //anonumys class
        /*
        Collections.sort(vectors, new Comparator<Vector2D>() {
            @Override
            public int compare(Vector2D o1, Vector2D o2) {
                if(o1.getX() - o2.getX()>0){
                    return 1;
                } else if(o1.getX() - o2.getX()<0){
                    return -1;
                }
                return  0;
            }
        });
        */
        Collections.sort(vectors, (o1,o2) -> {
            if(o1.getX() - o2.getX()>0){
                return 1;
            } else if(o1.getX() - o2.getX()<0){
                return -1;
            }
            return  0;

        });


    }

}
