import java.util.Comparator;

public class GeneticAlgStringComparator implements Comparator<String> {
    @Override
    public int compare(String o1, String o2) {
        if(GeneticAlg.getKoeff(o1)>GeneticAlg.getKoeff(o2)){
            return -1;
        } else if(GeneticAlg.getKoeff(o1)<GeneticAlg.getKoeff(o2)){
            return 1;
        } else return 0;
    }
}
