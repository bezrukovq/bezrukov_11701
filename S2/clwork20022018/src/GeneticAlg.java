import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class GeneticAlg {

    public static void GeneratePopulation(List<String> population, int n, int number) {
        for (int i = 0; i < number; i++) {
            population.add(GenerateString(n));
        }
    }

    public static String GenerateString(int n) {
        String str = "";
        for (int i = 0; i < n; i++) {
            str += (char) ('a' + Math.random() * 25);
        }
        return str;
    }

    public static void ShowPopulation(List<String> population) {
        for (String o : population) {
            System.out.print(o + getKoeff(o) + "     ");
        }
        System.out.println("");
    }

    //------------------
    public static void Selection(List<String> population, List<String> childPopulation, int n) {
        do {
            makeChildren(population, childPopulation, n);
            Collections.sort(childPopulation, new GeneticAlgStringComparator());
            System.out.println("Population:");
            ;
            ShowPopulation(population);
            System.out.println("Children");
            ShowPopulation(childPopulation);
            change(population, childPopulation);

        } while (!checkPopulation(population));

    }

    //--------------------
    public static void makeChildren(List<String> population, List<String> childPopulation, int n) {
        int parent;
        for (int i = 0; i < childPopulation.size(); i++) {
            childPopulation.set(i,"");
            for (int ch = 0; ch < n; ch++) {
                parent = (int) (Math.random() * 2);
                childPopulation.set(i, childPopulation.get(i) + population.get(2 * i + parent).charAt(ch));
            }
        }
    }

    /*
        public static void sort(String[] population) {
            String forChange = "";
            double fChange;
            double[] koef = new double[population.length];
            for (int i = 0; i < population.length; i++) {
                koef[i] = getKoeff(population[i]);
            }
            for (int i = population.length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (koef[j] < koef[j + 1]) {
                        forChange = population[j];
                        population[j] = population[j + 1];
                        population[j + 1] = forChange;
                        fChange = koef[j];
                        koef[j] = koef[j + 1];
                        koef[j + 1] = fChange;
                    }
                }
            }

        }
    */
    public static void change(List<String> population, List<String> childPopulation) {
        String forChange;
        for (int i = 0; i < childPopulation.size(); i++) {
            for (int j = 0; j < population.size() && i < childPopulation.size(); j++) {
                if (getKoeff(childPopulation.get(i)) > getKoeff(population.get(j))) {
                    forChange = population.get(j);
                    population.set(j, childPopulation.get(i));
                    childPopulation.set(i, forChange);
                    i++;
                }
            }
        }
    }

    public static boolean checkPopulation(List<String> population) {
        boolean check = true;
        for (String o : population) {
            check &= getKoeff(o) > 90.0;
        }
        return check;
    }

    public static double getKoeff(String population) {
        int numTrue = 0;
        int a;
        int b;
        for (int i = 0; i < population.length() - 1; i++) {
            a = (int) population.charAt(i);
            b = (int) population.charAt(i + 1);
            if (Math.abs(a - b) > 5) {
                numTrue += 25;
            }
        }

        return (double) numTrue;
    }

    public static void main(String[] args) {
        List<String> population = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        int n = 5;
        int number = sc.nextInt();
        List<String> childPopulation = new ArrayList<>();
        //String[] childPopulation = new String[number / 2];
        GeneratePopulation(population, n, number);
        GeneratePopulation(childPopulation, n, number/2);
        Collections.sort(population, new GeneticAlgStringComparator());
        Selection(population, childPopulation, n);
        System.out.println("   FINAL    ");
        System.out.println("Population:");
        ShowPopulation(population);

    }
}
