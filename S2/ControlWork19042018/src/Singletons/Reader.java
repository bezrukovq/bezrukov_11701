package Singletons;

import entities.Airline;
import entities.Airport;
import entities.Flight;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class Reader {

    private static String trim(String str) {
        return (String) Stream.of(str.split("'"))
                .filter((str1) -> !str1.equals(""))
                .toArray()[0]
                ;
    }


    public static List<Airline> readAirlineData(String fileName) {
        List<Airline> airlines;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(fileName)
                    )
            );
            airlines = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {

                    String[] strings = line.split(",");
                    airlines.add(
                            new Airline(
                                    Integer.parseInt(strings[0]),
                                    trim(strings[1]),
                                    trim(strings[2]),
                                    trim(strings[3])
                            )
                    );
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            airlines = null;
            e.printStackTrace();
        }
        return airlines;
    }

    public static List<Airport> readAirportData(String fileName) {
        List<Airport> airlports;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(fileName)
                    )
            );
            airlports = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {

                    String[] strings = line.split(",");
                    airlports.add(
                            new Airport(
                                    trim(strings[0]),
                                    trim(strings[1]),
                                    trim(strings[2]),
                                    trim(strings[3])
                            )
                    );
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            airlports = null;
            e.printStackTrace();
        }
        return airlports;
    }

    public static List<Flight> readFlightData(String fileName) {
        List<Flight> flights;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(fileName)
                    )
            );
            flights = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {

                    String[] strings = line.split(", ");
                    flights.add(
                            new Flight(
                                    AirlineCollectionSingleton.getInstance().stream().filter(a -> a.getId()== Integer.parseInt(strings[0])).findAny().get(),
                                    Integer.parseInt((strings[1])),
                                    AirportCollectionSingleton.getInstance().stream().filter(a-> a.getCode().equals(trim(strings[2]))).findAny().get(),
                                    AirportCollectionSingleton.getInstance().stream().filter(a-> a.getCode().equals(trim(strings[3]))).findAny().get()
                            )
                    );
                    line = bufferedReader.readLine();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            flights = null;
            e.printStackTrace();
        }
        return flights;
    }
}
