package Singletons;

import entities.Airline;

import java.util.List;

public class AirlineCollectionSingleton {
    private static final String FILE_NAME = "res/airlines.csv";
    private static List<Airline> instance;

    public static List<Airline> getInstance() {
        if (instance == null) {
            instance = Reader.readAirlineData(FILE_NAME);
        }
        return instance;
    }
}
