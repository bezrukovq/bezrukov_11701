package Singletons;

import entities.Airport;

import java.util.List;

public class AirportCollectionSingleton {
    private static final String FILE_NAME = "res/airports100.csv";
    private static List<Airport> instance;

    public static List<Airport> getInstance() {
        if (instance == null) {
            instance = Reader.readAirportData(FILE_NAME);
        }
        return instance;
    }
}
