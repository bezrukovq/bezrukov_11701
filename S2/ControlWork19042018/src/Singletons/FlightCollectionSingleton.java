package Singletons;

import entities.Flight;

import java.util.List;

public class FlightCollectionSingleton {
    private static final String FILE_NAME = "res/flights.csv";
    private static List<Flight> instance;

    public static List<Flight> getInstance() {
        if (instance == null) {
            instance = Reader.readFlightData(FILE_NAME);
        }
        return instance;
    }
}
