import Singletons.AirportCollectionSingleton;
import Singletons.FlightCollectionSingleton;
import entities.Airport;
import entities.Flight;

import java.util.List;
import java.util.stream.Collectors;

public class Task02 {
    public static void main(String[] args) {
        countFlightsLoopStyle();
        System.out.println();
        System.out.println(AirportCollectionSingleton.getInstance().stream()
                .collect(
                        Collectors.toMap(
                                x -> x,
                                x -> countFlights(x.getCode())
                        )
                ));
    }

    public static long countFlights(String airport) {
        return FlightCollectionSingleton.getInstance().stream()
                .filter(a -> a.getDestAirport().getCode().equals(airport) || a.getSourceAirport().getCode().equals(airport))
                .count();
    }

    private static void countFlightsLoopStyle() {
        List<Airport> airports = AirportCollectionSingleton.getInstance();
        int numairports = airports.size();
        List<Flight> flights = FlightCollectionSingleton.getInstance();
        int[] a = new int[numairports];
        for (Airport x : airports) {
            int numOfFlights = 0;
            for (Flight f : flights) {
                if(f.getSourceAirport().getCode().equals(x.getCode()))
                    numOfFlights++;
                if(f.getDestAirport().getCode().equals(x.getCode()))
                    numOfFlights++;
            }
            System.out.print(x.getName()+" ="+numOfFlights+", ");
        }
    }
}
