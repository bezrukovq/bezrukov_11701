import Singletons.AirlineCollectionSingleton;
import Singletons.FlightCollectionSingleton;
import entities.Airline;
import entities.Flight;

import java.util.List;

public class Task01 {
    public static void main(String[] args) {
        System.out.println(getAirlineStreamstyle());
        System.out.println(getAirlineLoopstyle());

    }

    private static String getAirlineLoopstyle() {
        int n = AirlineCollectionSingleton.getInstance().size();
        int[] arr = new int[n];
        List<Flight> flights = FlightCollectionSingleton.getInstance();
        for(Flight x : flights){
            arr[x.getAirline().getId()-1]++;
        }
        int a =0;
        for(int i=1; i<arr.length; i++){
            if(arr[i]>=arr[a]){
                a=i;
            }
        }

        return AirlineCollectionSingleton.getInstance().get(a).getName();
    }

    public static String getAirlineStreamstyle(){
        String airline = AirlineCollectionSingleton.getInstance().stream()
                .reduce((x1 ,x2)->{return getNumofFlights(x1.getId())>getNumofFlights(x2.getId()) ? x1 : x2;})
                .orElse(new Airline(0,"","",""))
                .getName();
        return airline;
    }

    public static long getNumofFlights(int airline){
        return FlightCollectionSingleton.getInstance().stream()
                .filter(a-> a.getAirline().getId()==airline)
                .count();
    }
}
