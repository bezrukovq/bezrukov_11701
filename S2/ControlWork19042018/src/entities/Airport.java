package entities;

public class Airport {
    private String city;
    private String code;
    private String name;
    private String country;

    public Airport(String city, String code, String name, String country) {
        this.city = city;
        this.code = code;
        this.name = name;
        this.country = country;
    }

    public String toString(){
        return name;
    }

    public String getCode() {
        return code;
    }


    public String getName() {
        return name;
    }

}
