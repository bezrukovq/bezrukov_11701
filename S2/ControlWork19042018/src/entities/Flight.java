package entities;

public class Flight {
    private Airline airline;
    private int id;
    private Airport sourceAirport;
    private Airport destAirport;

    public Flight(Airline airline, int id, Airport sourceAirport, Airport destAirport) {
        this.airline = airline;
        this.id = id;
        this.sourceAirport = sourceAirport;
        this.destAirport = destAirport;
    }

    public Airline getAirline() {
        return airline;
    }

    public Airport getSourceAirport() {
        return sourceAirport;
    }


    public Airport getDestAirport() {
        return destAirport;
    }


}
