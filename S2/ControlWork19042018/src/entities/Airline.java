package entities;

public class Airline {
    private int id;
    private String name;
    private String abb;
    private String country;

    public Airline(int id, String name, String abb, String country) {
        this.id = id;
        this.name = name;
        this.abb = abb;
        this.country = country;
    }

    public int getId() {
        return id;
    }


    public String getName() {
        return name;
    }

}
