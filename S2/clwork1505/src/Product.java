public class Product {
    String filename;
    boolean status = false;

    public Product(String filename) {
        this.filename = filename;
    }

    public boolean isReady() {
        return status;
    }
    public void produce(){
        System.out.println("producing");
        status = true;
    }
    public void use(){
        System.out.println("using");
        status = false;
    }


}
