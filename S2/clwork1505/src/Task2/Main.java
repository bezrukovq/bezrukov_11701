package Task2;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws IOException {
        URL url= null;
        try {
            url = new URL("https://en.wikipedia.org/wiki/London");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String ext = "pdf";

        (new Downloader(url,ext)).start();
    }
}
   /*
    Object lock = new Object();


    Downloader d = new Downloader(source, destination, lock) {
        new Thread(new Runnable() {
            lock.wait();
            new FileDownload(source).download()
            lock.notify();
        }.start();
    };

    Copier c = new Copier(destination, lock) {
        new Thread(new Runnable() {
            new FileCopier(destination).copy() {
                synchronized (lock) {
                    copy(destination, destination + " (1)");
                }
            };
        }.start();
    };
  }
  */
