public class Producer extends Thread {
    Product product;

    public Producer(Product product) {
        this.product = product;
    }

    public void run() {
        while (true) {
            synchronized (product) {
                while (!product.isReady()) {

                }
            }
        }
    }
}
