import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Tree {
    Node root;

    public Tree(int value) {
        this.root = new Node(value);
    }

    private Node addRecursive(Node current, int value) {
        if (current == null) {
            return new Node(value);
        }

        if (value < current.value) {
            current.left = addRecursive(current.left, value);
        } else if (value > current.value) {
            current.right = addRecursive(current.right, value);
        } else {
            return current;
        }

        return current;
    }

    public static void RecursiveInDepth(Node node) {
        if (node != null) {
            RecursiveInDepth(node.left);
            System.out.print(node.value + " ");
            RecursiveInDepth(node.right);
        }
    }

    public static void NonRecursiveDepth(Node root){
        Stack<Node> nodes = new Stack<>();
        nodes.push(root);
        while (!nodes.empty()){
            Node node = nodes.peek();
            boolean isLeaf = (node.right == null && node.left == null);
            if(!isLeaf){
                if(node.left!=null)
                nodes.push(node.left);
                if(node.right!=null)
                    nodes.push(node.right);
            } else {
                System.out.print(" " + node.value);
                nodes.pop();
            }

        }
    }

    public static void iterativePostOrderTraversal(Node root){
        Node cur = root;
        Node pre = root;
        Stack<Node> s = new Stack<Node>();
        s.push(root);
        while(!s.isEmpty()){
            cur = s.peek();
            if(cur==pre||cur==pre.left ||cur==pre.right){// we are traversing down the tree
                if(cur.left!=null){
                    s.push(cur.left);
                }
                else if(cur.right!=null){
                    s.push(cur.right);
                }
                if(cur.left==null && cur.right==null){
                    System.out.println(s.pop().value);
                }
            }else if(pre==cur.left){// we are traversing up the tree from the left
                if(cur.right!=null){
                    s.push(cur.right);
                }else if(cur.right==null){
                    System.out.println(s.pop().value);
                }
            }else if(pre==cur.right){// we are traversing up the tree from the right
                System.out.println(s.pop().value);
            }
            pre=cur;
        }
    }


    public static void inBreadth(Node root){
        Queue<Node> nodes = new LinkedList<>();
        nodes.add(root);

        while (!nodes.isEmpty()) {

            Node node = nodes.remove();

            System.out.print(" " + node.value);

            if (node.left != null) {
                nodes.add(node.left);
            }

            if (node.right != null) {
                nodes.add(node.right);
            }
        }
    }





    public void add(int value) {
        root = addRecursive(root, value);
    }

    public static void main(String[] args) {
        Tree tr = new Tree(5);
        tr.add(6);
        tr.add(7);
        tr.add(4);
        tr.add(2);
        tr.add(3);
        tr.add(1);
        Tree.RecursiveInDepth(tr.root);
        System.out.println();
        Tree.NonRecursiveDepth(tr.root);
    }
}
