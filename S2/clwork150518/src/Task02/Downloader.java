package Task02;

public class Downloader extends Thread {

    private File file;

    public Downloader(File file) {
        this.file = file;
    }

    @Override
    public void run() {
        synchronized (file) {
            while (file.isUsed()) {
                try {
                    file.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            file.download();
            file.notify();
        }
    }
}
